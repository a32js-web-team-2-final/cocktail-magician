import './App.css';
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer';
import { Routes, Route } from 'react-router-dom';
import AllBarsView from './views/AllBarsView/AllBarsView';
import SingleBarView  from './views/SingleBarView/SingleBarView';
import AllCocktailsView from './views/AllCocktailsView/AllCocktailsView';
import SingleCocktailView from './views/SingleCocktailView/SingleCocktailView';
import SignUpForm from './components/Sign up/SignUpForm';
import LogInForm from './components/Log in/LogInForm';
import { SnackbarProvider } from 'notistack';
import myTheme from './common/custom-theme';
import { ThemeProvider} from '@mui/material/styles';
import { useState, useEffect } from 'react';
import AuthContext from './common/contexts/auth-context';
import { getToken, getUserSession } from './common/contexts/auth-context';
import AdminDashboardView from './views/AdminDashboardView';
import HomeView from './views/HomeView';
import MapView from './views/MapView';
import jwt from 'jsonwebtoken';
import { getUserInfoMe } from './services/private-requests';
import NotFound from './views/NotFound';
import UserProfileContext from './common/contexts/user-profile-context';

const App = () => {

  const [auth, setAuth] = useState({
    user: getUserSession(),
    isLoggedIn: !!getUserSession()
  });

  const [userProfile, setUserProfile] = useState({userProfile: {}})

  useEffect(() => {

    try {
      const decoded = jwt.decode(getToken())

      getUserInfoMe(decoded.id)
      .then(userInfo => {
        
        setUserProfile({userProfile: userInfo});
      })
    } catch {

    }
  }, [userProfile])

  return (
    <div className="App">
    <ThemeProvider theme={myTheme}>
    <SnackbarProvider maxSnack={3}>
      <AuthContext.Provider value={{...auth, setAuth }}>
        <UserProfileContext.Provider value={{...userProfile, setUserProfile}}>
        <Header/>
        <Routes>
          <Route path="/" element={<HomeView/>}/>
          <Route auth={auth.isLoggedIn} path="/map" element={<MapView/>}/>
          <Route path="/bars" element={<AllBarsView />}/>
          <Route path="/cocktails" element={<AllCocktailsView/>} />
          <Route path="bars/:id" element={<SingleBarView />} />
          <Route path="/cocktails/:id" element={<SingleCocktailView />} />
          <Route path="*" element={<NotFound />} />
          <Route auth={auth.isLoggedIn && userProfile.role === 'magician'} path="/admin" element={<AdminDashboardView/>} />
          <Route auth={!auth.isLoggedIn} path="/sign-up" element={<SignUpForm />} />
          <Route auth={!auth.isLoggedIn} path="/log-in" element={<LogInForm/>} />
        </Routes>
        <Footer />
        </UserProfileContext.Provider>
      </AuthContext.Provider>
    </SnackbarProvider>
    </ThemeProvider>
    </div>
    
  );
}

export default App;
