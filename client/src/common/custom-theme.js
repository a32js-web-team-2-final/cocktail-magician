// Custom color pallette for MUI theme
import { createTheme } from '@mui/material/styles';

const myTheme = createTheme({
  palette: {
    primary: {
      main: '#3D3C3A',
    },
    secondary: {
      main: '#ADFF2F',
    },
  },
});

export default myTheme;

// '#3D3C3A'
// #ADFF2F