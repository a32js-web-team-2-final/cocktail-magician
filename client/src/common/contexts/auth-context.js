import { createContext } from 'react';
import jwt from 'jsonwebtoken';

const AuthContext = createContext({
  user: {},
  isLoggedIn: false,
  setAuth: () => {},
});

export const getToken = () => localStorage.getItem("token") || "";

export const getUserSession = () => {
  try {
    const { id: sub, ...user } = jwt.decode(getToken());

    return user;
  } catch {
    return null;
  }
};

export default AuthContext;
