import { createContext } from 'react';

const UserProfileContext = createContext({
  userProfile: {
    username: '',
    displayName: '',
    role: '',
    favouriteCocktails: [],
    reviewedBars: [],
  },
  setUserProfile: () => {}
})

export default UserProfileContext;