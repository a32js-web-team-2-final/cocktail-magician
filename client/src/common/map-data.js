export const center = {
  lat: 42.6964955,
  lng: 23.3248505
};

export let coords =[
  {
    id: 1,
    name: 'Bar Test Name',
    lat: 42.698325,
    lng: 23.3401904
  },
  { 
    id: 2,
    name: 'Largo',
    lat: 42.6980667,
    lng: 23.320518
  },
  {
    id: 3,
    name: 'Sputnik Cocktail Bar',
    lat: 42.698325,
    lng: 23.3401904
  },
  { 
    id: 4,
    name: 'The Cocktail Bar',
    lat: 42.6922787,
    lng: 23.3202277
  },
  {
    id: 5,
    name: 'OSCAR Club',
    lat: 42.6926542,
    lng: 23.3277672
  },
  { 
    id: 6,
    name: 'The Scene Rooftop Bar',
    lat: 42.696461,
    lng: 23.3334574
  },
  {
    id: 7,
    name: 'Once Upon A Time Biblioteka',
    lat: 42.69516,
    lng: 23.3337439
  },
  { 
    id: 8,
    name: 'Tobacco Bar',
    lat: 42.6964955,
    lng: 23.3248505
  },
  {
    id: 9,
    name: 'Tell Me Bar',
    lat: 42.6927393,
    lng: 23.325697
  },
  { 
    id: 10,
    name: 'Social Cafe Bar',
    lat: 42.693443,
    lng: 23.3179789
  },
  {
    id: 11,
    name: 'ShiSha Bar SpeShial',
    lat: 42.6769714,
    lng: 23.3557162
  },
  { 
    id: 12,
    name: 'Hambara Bar',
    lat: 42.6899485,
    lng: 23.3256834
  },
  { id: 13,
    name: 'One More Bar',
    lat: 42.6923943,
    lng: 23.3285681
  },
  { 
    id: 14,
    name: 'Culture Beat Club',
    lat: 42.684273,
    lng: 23.317145
  },
  { 
    id: 15,
    name: 'Bar Friday',
    lat: 42.6926189,
    lng: 23.3254938
  }];
