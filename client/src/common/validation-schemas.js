// YUP Validation Schemas
import * as Yup from 'yup';
import { minLengthUsername, maxLengthUsername, minLengthDisplayName, maxLengthDisplayName, minLengthIngredient, maxLengthIngredient, minLengthPassword, maxLengthPassword, minLengthBarName, maxLengthBarName, minLengthBarAddress, maxLengthBarAddress, minLengthBarPhone, maxLengthBarPhone, minLengthCocktailName, maxLengthCocktailName, minLengthReviewText, maxLengthReviewText} from './constants';

/**
 * SIGN UP: Validation Schema
 */
export const signUpValidationSchema = Yup.object().shape({
  username: Yup.string()
    .min(minLengthUsername, 'Username is too short!')
    .max(maxLengthUsername, 'Username is too long!')
    .required('Username is required'),
  displayName: Yup.string()
    .min(minLengthDisplayName, 'Display name is too short!')
    .max(maxLengthDisplayName, 'Display name is too long!'),
  password: Yup.string()
    .min(minLengthPassword, 'Password is too short!')
    .max(maxLengthPassword, 'Password is too long!')
    .required('Password is required')
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$/,
      "Password must contain at least one letter, one number, and one special character."
    ),
});

/**
 * LOG IN: Validation Schema
 */
export const logInValidationSchema = Yup.object().shape({
  username: Yup.string()
    .min(minLengthUsername, 'Username is too short!')
    .max(maxLengthUsername, 'Username is too long!')
    .required('Username is required'),
  password: Yup.string()
    .min(minLengthPassword, 'Password is too short!')
    .max(maxLengthPassword, 'Password is too long!')
    .required('Password is required')
});

/**
 * BAR: Validation Schema
 */
export const barValidationSchema = Yup.object().shape({
  name: Yup.string()
    .min(minLengthBarName, 'Bar name is too short!')
    .max(maxLengthBarName, 'Bar name is too long!')
    .required('Bar name is required'),
  address: Yup.string()
    .min(minLengthBarAddress, 'Address is too short!')
    .max(maxLengthBarAddress, 'Address is too long!')
    .required('Address is required'),
  phone: Yup.string()
    .min(minLengthBarPhone, 'Phone number is too short!')
    .max(maxLengthBarPhone, 'Phone number is too long!')
    .required('Phone number is required')
});

/**
 * COCKTAIL: Validation Schema
 */
export const cocktailValidationSchema = Yup.object().shape({
  name: Yup.string()
  .min(minLengthCocktailName, 'Cocktail name is too short!')
  .max(maxLengthCocktailName, 'Cocktail name is too long!')
  .required('Cocktail name is required'),
});

/**
 * INGREDIENT: Validation Schema
 */
export const ingredientValidationSchema = Yup.object().shape({
  name: Yup.string()
  .min(minLengthIngredient, 'Ingredient name is too short!')
  .max(maxLengthIngredient, 'Ingredient name is too long!')
  .required('Ingredient name is required'),
});

/**
 * REVIEW: Validation Schema
 */
export const reviewValidationSchema = Yup.object().shape({
  text: Yup.string()
    .min(minLengthReviewText, 'Review is too short!')
    .max(maxLengthReviewText, 'Review is too long!')
    .required('Review is required'),
});

/**
 * DISPLAY NAME: Validation Schema
 */
export const displayNameValidationSchema = Yup.object().shape({
  displayName: Yup.string()
    .min(minLengthDisplayName, 'Display name is too short!')
    .max(maxLengthDisplayName, 'Display name is too long!')
});
