export const API = 'http://localhost:5000/';
export const googleMapsApiKey = 'AIzaSyC4BeD07qPNnzq28c5-zxra8l_YCqTlOGU';

// Username
export const minLengthUsername = 3;
export const maxLengthUsername = 25;

// Password
export const minLengthPassword = 3;
export const maxLengthPassword = 25;

// Display Name
export const minLengthDisplayName = 3;
export const maxLengthDisplayName = 25;

// Review: Text
export const minLengthReviewText = 25;
export const maxLengthReviewText = 1000;

// Review: Rating
export const minLengthReviewRating = 1;
export const maxLengthReviewRating = 5;

// Bar: Name
export const minLengthBarName = 5;
export const maxLengthBarName = 50;

// Bar: Phone
export const minLengthBarPhone = 2;
export const maxLengthBarPhone = 20;

// Bar: Address
export const minLengthBarAddress = 5;
export const maxLengthBarAddress = 100;

// Cocktail: Name
export const minLengthCocktailName = 2;
export const maxLengthCocktailName = 50;

// Ingredient: Name
export const minLengthIngredient = 2;
export const maxLengthIngredient = 50;

