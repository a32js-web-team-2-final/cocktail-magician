export const customSelectStyle = {
  control: base => ({
    ...base,
    height: 45,
    minHeight: 45,
  })
};