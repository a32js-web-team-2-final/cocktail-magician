import { API } from '../common/constants';

export const signUp = () => {

}
export const logIn = () => {

}

export const searchBarsByName = (name) => {
  
  return fetch(`${API}bars?name=${name}`)
          .then(data => data.json())
}

export const searchBarsByCocktailIngredient = (queries) => {

  return fetch(`${API}bars/cocktail-ingredients?values=${queries}`)
  .then(data => data.json())
}
export const getBarDetails = (id) => {
  
  return (
    fetch(`${API}bars/${id}`)
    .then(res => res.json())
  )
}

export const getBarReviews = (id) => {

  return (
    fetch(`${API}bars/${id}/reviews`)
    .then(data => data.json())
  )
}

export const getUserById = (id) => {
  
  return (
    fetch(`${API}users/${id}`)
    .then(data => data.json())
  )
}

export const getAllCocktails = () => {

  return (
    fetch(`${API}cocktails`)
    .then(data => data.json())
  )
}

export const getCocktailDetails = (cocktailId) => {

  return (
    fetch(`${API}cocktails/${cocktailId}`)
    .then(data => data.json())
  )
}

