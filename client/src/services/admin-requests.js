import { getToken } from '../common/contexts/auth-context';
import { API } from '../common/constants';


/**
 * POST: creates a new Bar with name, phone, address, and at least 1 image
 * @param {{FormData}} formData 
 * @returns {{Bar}} Bar obj or Error obj
 */
export const createBar = (formData) => {

  return fetch(`${API}bars`, {
    method: 'POST',
    headers: {
      'Authorization' : `Bearer ${getToken()}`
    },
    body: formData
  })
  .then(data => data.json())
};

/**
 * PUT: updates bar details = name, address, phone
 * @param {number} barId 
 * @param {{string}} updates 
 * @returns {{Bar}} Bar obj or Error obj
 */
export const editBarDetails = (barId, updates) => {

  return fetch(`${API}bars/${barId}`, {
    method: 'PUT',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
      'Content-type': 'application/json'
    },
    body: JSON.stringify(updates)
  })
  .then(data => data.json())
};

/**
 * POST: creates a new cocktail with name,image, and a list of existing ingredients
 * @param {*} formData 
 * @returns {{Cocktail}} Cocktail obj or Error obj
 */
export const createCocktail = (formData) => {

  return fetch(`${API}cocktails`, {
    method: 'POST',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
    },
    body: formData
  })
  .then(data => data.json())
};

/**
 * PUT: updates cocktail details - name or image
 * @param {number} cocktailId 
 * @param {{FormData}} formData
 * @returns {{Cocktail}} Cocktail obj or Error obj
 */
export const editCocktailDetails = (cocktailId, updates) => {

  return fetch(`${API}cocktails/${cocktailId}`, {
    method: 'PUT',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
    },
    body: updates
  })
  .then(data => data.json())
};

/**
 * PUT: adds ingredients to a cocktail after it is created
 * @param {number} cocktailId 
 * @param {[ingredient]} ingredients arr
 * @returns {{Cocktail}} Cocktail obj or Error message
 */
export const addIngredients = (cocktailId, ingredients) => {

  return fetch(`${API}cocktails/${cocktailId}/ingredients`, {
    method: 'PUT',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
      'Content-type': 'application/json'
    },
    body: JSON.stringify({'ingredients' : ingredients})
  })
  .then(data => data.json())
};

/**
 * DELETE: deletes existing ingredients from a cocktail
 * @param {number} cocktailId 
 * @param {[string]} ingredients 
 * @returns {{Cocktail}} Cocktail obj or Error message
 */
export const removeIngredients = (cocktailId, ingredients) => {

  return fetch(`${API}cocktails/${cocktailId}/ingredients`, {
    method: 'DELETE',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
      'Content-type': 'application/json'
    },
    body: JSON.stringify({'ingredients' : ingredients})
  })
  .then(data => data.json())
};

/**
 * GET: get all bars
 * @returns {[Bar]} arr of {Bar} or Error message
 */
export const getAllBars = () => {

  return fetch(`${API}bars`, {
    method: 'GET'
  })
    .then(data => data.json())
};

/**
 * GET: get bar details from bar ID
 * @param {number} barId 
 * @returns {{Bar}} Bar obj or Error message
 */
export const getBarDetails = (barId) => {
  if(barId === undefined) {
    return;
  } else {
    return fetch(`${API}bars/${barId}`)
    .then(data => data.json())
  }
};

/**
 * GET: get all created cocktails
 * @returns {[{Cocktails}]} arr of {Cocktail} or Error message
 */
export const getAllCocktails = () => {

  return fetch(`${API}cocktails`, {
    method: 'GET'
  })
    .then(data => data.json())
};

/**
 * GET: list of all created ingredients
 * @returns {[Ingredients]} Ingredients arr or Error message
 */
export const getAllIngredients = () => {

  return fetch(`${API}ingredients`, {
    method: 'GET',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
      'Content-type': 'application/json'
    }
  })
  .then(data => data.json())
};

/**
 * PUT: adds an existing cocktail to an existing bar
 * @param {number} barId 
 * @param {number} cocktailId 
 * @returns {Bar} Bar obj or Error obj
 */
export const addCocktailToBar = (barId, cocktailId) => {

  return fetch(`${API}bars/${barId}/cocktails/${cocktailId}`, {
    method: 'PUT',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
      'Content-type': 'application/json'
    }
  })
    .then(response => response.json())
    .then(data => data.json())
};

/**
 * DELETE: removes an existing cocktail from an existing bar
 * @param {number} barId 
 * @param {number} cocktailId 
 * @returns {{Bar}} Bar obj or Error message
 */
export const removeCocktailFromBar = (barId, cocktailId) => {
  return fetch(`${API}bars/${barId}/cocktails/${cocktailId}`, {
    method: 'DELETE',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
      'Content-type': 'application/json',
    }
  })
  .then(data => data.json())
};

/**
 * PUT: updates visibility status (isVisible) of an image from the bar gallery
 * @param {number} barId 
 * @param {number} imageId 
 * @param {string} action : 'hide' by default or 'show'
 * @returns {{Bar}} Bar obj or Error message
 */
export const toggleImageVisibility = (barId, imageId, action = 'hide') => {
  
  return fetch(`${API}bars/${barId}/images/${imageId}`, {
    method: 'PUT',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
      'Content-type': 'application/json'
    },
    body: JSON.stringify({"action": `${action}`})
  })
  .then(data => data.json())
};

/**
 * PUT: sets bar cover image (isCover: 1)
 * @param {number} barId 
 * @param {number} imageId 
 * @returns {{Bar}} Bar obj or Error message
 */
export const setCoverImage = (barId, imageId) => {
  
  return fetch(`${API}bars/${barId}/images/${imageId}/cover`, {
    method: 'PUT',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
    }
  })
  .then(data => data.json())
};

/**
 * POST: adds more images to bar after it is created
 * @param {number} barId 
 * @param {{FormData}} formData
 * @returns {{Bar}} Bar obj or Error message
 */
export const uploadMoreBarImages = (barId, formData) => {
  return fetch(`${API}bars/${barId}/images`, {
    method: 'POST',
    headers: {
      'Authorization' : `Bearer ${getToken()}`
    },
    body: formData
  })
  .then(data => data.json())
};