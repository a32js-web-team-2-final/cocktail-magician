import { getToken } from '../common/contexts/auth-context';
import { API } from '../common/constants';

export const getUserInfoMe = () => {

  return fetch(`${API}users/me`, {
    headers: {
      'Authorization' : `Bearer ${getToken()}`
    }
  })
  .then(data => data.json());
}
export const editDisplayName = (values) => {
  console.log(`${JSON.stringify(values)}`)
  return fetch('http://localhost:5000/users', {
    method: 'PUT',
    headers: {
      'Authorization' : `Bearer ${getToken()}`,
      'Content-type': 'application/json'
    }, 
    body: JSON.stringify(values)
  })
  .then(data => data.json())
};

export const addReview = (barId, review) => {
  
  return fetch(`http://localhost:5000/bars/${barId}/reviews`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${getToken()}`,
        'Content-type': 'application/json'
      },
      body: JSON.stringify(review)
    })
    .then(data => data.json())
};

export const addFavouriteCocktail = (cocktailId) => {

  return fetch(`${API}users/cocktails/${cocktailId}`, {
    method: 'PUT',
    headers: {
      'Authorization' : `Bearer ${getToken()}`
    }
  })
}

export const removeFavouriteCocktail = (cocktailId) => {

  return fetch(`${API}users/cocktails/${cocktailId}`, {
    method: 'DELETE',
    headers: {
      'Authorization' : `Bearer ${getToken()}`
    }
  })
}