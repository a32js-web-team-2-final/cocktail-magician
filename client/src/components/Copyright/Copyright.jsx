import React from 'react';
import { Typography, Link } from '@mui/material';

const Copyright = () => {
  
  return (
    <Typography variant="body2" color="white">
      {'Copyright © '}
      <Link color="inherit" href="https://gitlab.com/a32js-web-team-2-final/cocktail-magician">
        Barology
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default Copyright;
