import React, { useContext } from 'react';
import { AppBar, Toolbar, Fab, IconButton, Box} from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import AuthContext from '../../common/contexts/auth-context';
import UserProfileContext from '../../common/contexts/user-profile-context';
import UserTabPanelDialog from '../User/UserProfileTab';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import LogoutIcon from '@mui/icons-material/Logout';
import Chip from '@mui/material/Chip';
import { useNavigate } from 'react-router';

const Header = () => {
  const { isLoggedIn, setAuth } = useContext(AuthContext);
  const { userProfile } = useContext(UserProfileContext);
  const navigate = useNavigate();

  const logout = () => {
    localStorage.removeItem('token')
    setAuth({ user: null, isLoggedIn: false});
    navigate('/');
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar sx={{paddingLeft: "50px", paddingRight: "50px"}}>
        
          <NavLink to="/" style={{color: "white", fontWeight: 600, fontSize: "1.3rem", marginLeft: "20px", marginRight: "20px", textDecoration: "none"}}><img height="90" src={`${process.env.PUBLIC_URL}/assets/logo.png`}  alt="barology-logo" border="0" /></NavLink>
        { isLoggedIn && 
          <Fab component={NavLink} to="/map" variant="extended" sx={{ mr: 3 }}>
              <LocationOnIcon sx={{ mr: 1 }} /> map 
          </Fab>
        }
          <NavLink to="/bars" style={{color: "white", fontWeight: 600, fontSize: "1.3rem", marginLeft: "20px", marginRight: "20px", textDecoration: "none"}}> BARS </NavLink>
          <NavLink to="/cocktails" style={{color: "white", fontWeight: 600, fontSize: "1.3rem", marginLeft: "5px", marginRight: "25px", textDecoration: "none"}}> COCKTAILS</NavLink>
          
          <Box sx={{ flexGrow: 1 }} />

          {(isLoggedIn && userProfile.role === 'magician') && 
          <NavLink to='/admin'>
            <Chip label='magician' sx={{bgcolor: "rgb(213, 213, 213)", fontSize: '0.9rem', fontFamily: 'Montserrat', marginLeft: "5px", marginRight: "5px"}} />
          </NavLink>
          }
          {isLoggedIn ?
          <Box sx={{ display: { xs: 'none', md: 'flex' }, gap: "12px"}}>
            {userProfile.username && 
            <UserTabPanelDialog user={userProfile}/>
            }
            <IconButton className="logout-btn" onClick={logout}>
              <LogoutIcon />
            </IconButton>
          </Box>
          :
          <>
          <NavLink to="/log-in" style={{fontSize: "1.1rem", color: "white", textDecoration: "none", marginRight: "5px"}}> Log in </NavLink>
          <Chip className="sign-up-chip" component={NavLink} to="/sign-up" label="Sign up" style={{color: "black", fontSize: "1.1rem", marginLeft: "10px", marginRight: "10px"}}/> 
          </>
          }
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;