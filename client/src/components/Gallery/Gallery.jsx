import React from 'react';
import { ImageList, ImageListItem } from '@material-ui/core';

const Gallery = ({bar, images}) => {

  return(
      <ImageList sx={{ width:800, height: 'auto' }} rowHeight={'auto'} cols={3} style={{'marginBottom': '10px', 'paddingLeft': '60px', 'paddingRight': '60px'}}>
        {images
          .filter(image => image.isVisible)
          .map((image) => (
          <ImageListItem key={image.id} style={{height: "360px"}}>
            <img
              src={`http://localhost:5000${image.url}`}
              alt={`${bar.name}-img-${image.id}`}
              loading="lazy"
              width="100%"
              height="360"
            />
          </ImageListItem>
        ))}
      </ImageList>
  );
};

export default Gallery;