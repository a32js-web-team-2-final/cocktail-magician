import React from 'react';
import Rating from '@mui/material/Rating';
import {useState} from 'react';

const ReviewRating = () => {
  const [value, setValue] = useState(2)

  return (
    <Rating name="review-rating" value={value} />
  );
}

export default ReviewRating;