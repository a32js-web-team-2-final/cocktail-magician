import * as React from 'react';
import Rating from '@mui/material/Rating';
import {useState} from 'react';
import { maxLengthReviewRating } from '../../common/constants';

const BarRating = ({rating}) => {
  return (
    <Rating name="bar-rating" value={rating || 0} readOnly/>
  );
}

export default BarRating;