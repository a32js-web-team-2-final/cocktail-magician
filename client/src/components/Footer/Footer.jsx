import React from "react";
import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import Copyright from '../Copyright/Copyright';

const Footer = () => {

  return (
    <Box sx={{
      bgcolor: "primary.main",
      px: 2,
      py: 2,
      width: "100%",
      position: "float",
      bottom: 0}}>
      <div>
        <Typography variant="body1" color="white">
            Check out our full project in our GitLab repo.
        </Typography>
        <img src="https://www.pngkey.com/png/full/979-9791155_svg-gitlab-logo-png.png" alt="gitlab-logo" style={{height: "34px", width: "auto"}}></img>
        <Copyright />
      </div>
    </Box>
  )
}

export default Footer;