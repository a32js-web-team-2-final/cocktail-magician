import React, { useState} from "react";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    width: "100%",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

const SearchInput = ({checked, ingredients, setIngredients, name, setName}) => {
  const [searchWord, setSearchWord] = useState('')

  const updateSearch= (e) => {

    if (!searchWord) {
      setName('')
      setIngredients('')
    }

    if (e.key === "Enter") {

      if(!checked) {
        setName(searchWord);
      } else {
        setIngredients(searchWord)
      }
      
    } else {
      setSearchWord(e.target.value)
    }
  }

  return (
    <Search sx={{display: "flex", marginTop: "50px", marginBottom: "20px", border: "1px solid hsl(0, 0%, 80%)"}}>
      <SearchIconWrapper>
        <SearchIcon />
      </SearchIconWrapper>
      <StyledInputBase
        placeholder="Search bars…"
        inputProps={{ "aria-label": "search" }}
        value={searchWord}
        onChange={updateSearch}
        onKeyUp={updateSearch}
      />
    </Search>
  );
};


export default SearchInput;