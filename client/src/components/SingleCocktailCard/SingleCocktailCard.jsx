import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box'
import { Chip } from '@mui/material';
import Checkbox from "@mui/material/Checkbox";
import FavoriteBorder from '@mui/icons-material/FavoriteBorder';
import Favorite from '@mui/icons-material/Favorite';
import { useContext } from 'react';
import AuthContext from '../../common/contexts/auth-context';
import UserProfileContext from '../../common/contexts/user-profile-context';
import { Link } from 'react-router-dom';
import { addFavouriteCocktail, removeFavouriteCocktail } from '../../services/private-requests';
import UserProfileTab from '../User/UserProfileTab';


const SingleCocktailCard = ({cocktail, isFavourite, setIsFavourite, favouritedBy}) => {
  const { isLoggedIn } = useContext(AuthContext);
  const { userProfile, setUserProfile } = useContext(UserProfileContext)

  const handleFavourite = () => {

    if (isFavourite) {
      removeFavouriteCocktail(cocktail.id)
      .then(() => {
      const target = userProfile.favouriteCocktails.filter(favourite => favourite.id === cocktail.id)[0];
      const updated = userProfile.favouriteCocktails.filter(cocktail => cocktail !== target);
      setUserProfile(prev => ({userProfile: {...prev.userProfile, favouriteCocktails: updated}}))
      setIsFavourite(false)
      })
    } else {
      addFavouriteCocktail(cocktail.id)
      .then(() => {
      const newFavourite = {id: cocktail.id, name: cocktail.name}
      const updated = [...userProfile.favouriteCocktails, newFavourite]
      setUserProfile(prev => ({userProfile: {...prev.userProfile, favouriteCocktails: updated}}))
      setIsFavourite(true);
      })
    }
  }

  return (
    <Card sx={{ maxWidth: "50%", maxHeight: '80%', margin: "0 auto"}} style={{backgroundColor: "transparent"}}>
        <CardMedia
          component="img"
          alt={`${cocktail.name}-cover`}
          height="auto-fit"
          width='100%'
          image={`http://localhost:5000${cocktail.imageUrl}`}
        />
        <CardContent>
          <Box sx={{display: "flex", justifyContent: "space-between"}}>
            <Typography sx={{textAlign: "left"}} gutterBottom variant="h6" component="div">
              {cocktail.name}
            </Typography>
            {isLoggedIn && 
            <Checkbox checked={isFavourite} icon={<FavoriteBorder />} checkedIcon={<Favorite />} onChange={handleFavourite}/>
            }
          </Box>
          <Typography sx={{textAlign: "left", marginBottom: "15px"}}>Ingredients:</Typography>
          <Box sx={{display: "flex", flexWrap: "wrap", gap: "2px", marginBottom: "15px"}}>
          {cocktail.ingredients &&
          cocktail.ingredients.map(ingredient => (
          <Chip key={ingredient.id} label={ingredient.name}/>
          ))}
          </Box>
          <Typography sx={{textAlign: "left", marginBottom: "15px"}}>Available in:</Typography>
          <Box sx={{display: "flex", flexWrap: "wrap", gap: "2px", marginBottom: "15px"}}>
          {cocktail.availableIn &&
          cocktail.availableIn.map(bar => (
            <Link key={bar.id} to={`/bars/${bar.id}`}>
              <Chip label={bar.name}/>
            </Link>
          ))}
          </Box>
          {favouritedBy.length ?
          <Typography sx={{textAlign: "left", marginBottom: "15px"}}>Favourite of:</Typography>
          :
          <Chip label='Not in user favorites yet.' variant='outlined'></Chip>
          }
          <Box sx={{display: "flex", flexWrap: "wrap", gap: "2px", marginBottom: "15px"}}>
          {favouritedBy &&
          favouritedBy.map(user => (
            <UserProfileTab key={user.id} user={user} />
          ))}
          </Box>
        </CardContent>
    </Card>
  )
};
export default SingleCocktailCard;