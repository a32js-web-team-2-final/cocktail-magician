import React from 'react';
import BarRating from '../Rating/BarRating';
import { Typography, Button } from '@material-ui/core';
import {useNavigate } from 'react-router-dom';
import { Box } from '@mui/system';

const BarListItem = ({bar}) => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(`/bars/${bar.id}`)
  }
  return (
    <Box className="bar-list-item">
      <Box className="bar-info">
        <Typography className="bar-name" variant="h5">{bar.name}</Typography>
        <Box className="bar-rating">
          <BarRating rating={bar.rating}/>
        </Box>
        <Box className="bar-address">
          <Typography>Address: {bar.address}</Typography>
        </Box>
        <Box className="bar-phone">
          <Typography>Phone: {bar.phone}</Typography>
        </Box>
        <Button variant="contained" className="bar-more-btn" onClick={handleClick}>View more</Button>
      </Box>
      <Box className='bar-list_cover-image'>
        <img src={`http://localhost:5000${bar.url}`} alt="bar-cover" style={{width: "100%", height: "100%"}}></img>
      </Box>
    </Box>
  )
}

export default BarListItem;