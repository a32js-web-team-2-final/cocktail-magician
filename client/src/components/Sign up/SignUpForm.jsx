import React from "react";
import {signUpValidationSchema } from "../../common/validation-schemas";
import Avatar from '@mui/material/Avatar';
import { Button, Link, Grid, Box, Typography} from "@material-ui/core";
import Container from '@mui/material/Container';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { TextField } from "@mui/material";
import { useFormik } from 'formik';
import { useSnackbar } from 'notistack';
import { API } from "../../common/constants";

export const SignUpForm = () => {
  
  const { enqueueSnackbar} = useSnackbar();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      username: '',
      displayName: '',
      password: '',
    },
    validationSchema: signUpValidationSchema,
    onSubmit: (values) => {

      console.log(values.password, values.username)
      fetch(`${API}users`,{
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(data => data.json())
      .then(data => {
        if (data.message) {
          console.log(`${data.message}`)
          enqueueSnackbar('Oops! There was an error. Please, try again.', { 
            variant: 'error',
          });
          throw new Error(data.message)
        } else {
          console.log(data)
          enqueueSnackbar('You have successfully registered. Please, proceed to log in page!', { 
            variant: 'success',
          });
        } 
      })
      .catch(e => console.log(e))
      formik.resetForm()
    },
  });

  return (
      <Container component="main" maxWidth="xs" height="auto">
        <Box
          sx={{
            marginTop: 28,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            height: 820,
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography variant="h4">
            Sign up
          </Typography>
          <Box component="form" onSubmit={formik.handleSubmit} sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoFocus
              value={formik.values.username}
              onChange={formik.handleChange}
              error={formik.touched.username && Boolean(formik.errors.username)}
              helperText={formik.errors.username && String(formik.errors.username)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              id="password"
              type="password"
              value={formik.values.password}
              onChange={formik.handleChange}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.errors.password && String(formik.errors.password)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 6 }}
            >
              Sign up
            </Button>
              <Grid item>
                <Link href="/log-in" variant="body2">
                  {"Already have an account? Log in here."}
                </Link>
              </Grid>
          </Box>
        </Box>
      </Container>
  );
};

export default SignUpForm;
