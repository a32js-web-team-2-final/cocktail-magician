import * as React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const Loader = () => {

  return (
    <Box sx={{ display: 'flex', position: "fixed", top: "50%", left: "50%", width: "200px", height: "200px"}}>
      <CircularProgress color="info" size={100}/>
    </Box>
  );
}

export default Loader;