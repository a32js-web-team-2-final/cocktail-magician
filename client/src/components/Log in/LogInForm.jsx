import React, { useContext } from "react";
import { Button, Link, Grid, Box, Typography, Avatar } from "@material-ui/core";
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { useFormik } from 'formik';
import { useSnackbar } from 'notistack';
import { API } from "../../common/constants";
import jwt from 'jsonwebtoken'; 
import AuthContext from "../../common/contexts/auth-context";
import UserProfileContext from "../../common/contexts/user-profile-context";
import { logInValidationSchema } from "../../common/validation-schemas";
import { getUserInfoMe } from '../../services/private-requests';
import { useNavigate } from "react-router";

export const LogInForm = ({history}) => {
  const {setAuth} = useContext(AuthContext);
  const { setUserProfile }= useContext(UserProfileContext);
  const { enqueueSnackbar} = useSnackbar();
  const navigate = useNavigate();


  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      username: '',
      password: '',
    },
    validationSchema: logInValidationSchema,
    onSubmit: (values) => {

      fetch(`${API}users/login`, {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(data => data.json())
      .then(data => {
        if (data.message) {
          console.log(`${data.message}`)
          enqueueSnackbar('Oops! There was an error. Please, try again.', { 
            variant: 'error',
          });
          throw new Error(data.message)
        } 

        const token = data.token;
        
        try {
          const payload = jwt.decode(token);
          localStorage.setItem('token', token);

          enqueueSnackbar('You have successfully logged in! Happy browsing :)', { 
            variant: 'success',
          }); 
          
          return payload;
        } catch {
          throw new Error('Something went wrong!');
        }
      })
      .then(payload => {
        getUserInfoMe(payload.sub)
        .then(userInfo => setUserProfile({userProfile: userInfo}))
        .then(() =>  setAuth({ user: { id: payload.sub, iat: payload.iat, exp: payload.exp }, isLoggedIn: true}))
      })
      .then(() => navigate('/') )
      .catch(e => console.log(e))

      formik.resetForm()
    }
  });


  return (
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 28,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            height: 820,
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography variant="h4" color='primary.main'>
            Log in
          </Typography>
          <Box component="form" onSubmit={formik.handleSubmit} sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              autoFocus
              value={formik.values.username}
              onChange={formik.handleChange}
              error={formik.touched.username && Boolean(formik.errors.username)}
              helperText={formik.errors.username && String(formik.errors.username)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              id="password"
              type="password"
              autoComplete="current-password"
              value={formik.values.password}
              onChange={formik.handleChange}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.errors.password && String(formik.errors.password)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Log in
            </Button>
              <Grid item>
                <Link href="/sign-up" variant="body2">
                  {"First time with us? Sign up here."}
                </Link>
              </Grid>
          </Box>
        </Box>
      </Container>

  );
};

export default LogInForm;