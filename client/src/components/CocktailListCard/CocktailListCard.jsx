import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardActionArea from '@mui/material/CardActionArea';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box'
import Chip from '@mui/material/Chip';
import { useNavigate } from 'react-router-dom';

const CocktailListCard = ({cocktail}) => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(`/cocktails/${cocktail.id}`)
  }

  return (
    <Card sx={{ maxWidth: 342, height: 680}} style={{backgroundColor: "transparent"}}>
      <CardActionArea onClick={handleClick}>
        <CardMedia gutterBottom
          component="img"
          alt={`${cocktail.name}-cover`}
          height="400"
          width='90%'
          image={`http://localhost:5000${cocktail.imageUrl}`}
        />
        </CardActionArea>
        <CardContent>
          <Box>
            <Typography sx={{textAlign: "left"}} gutterBottom variant="h6" component="div">
              {cocktail.name}
            </Typography>
          </Box>
          <Typography variant="subtitle2" sx={{textAlign: "left", marginBottom: "15px"}}>Ingredients:</Typography>
          <Box sx={{display: "flex", flexWrap: "wrap", gap: "2px", marginBottom: "15px"}}>
          {cocktail.ingredients &&
          cocktail.ingredients.map(ingredient => (
          <Chip key={ingredient.id} label={ingredient.name}/>
          ))}
          </Box>
          {
            cocktail.favouriteCount ?
            <Chip variant='outlined' label={`Loved by ${cocktail.favouriteCount} users`}></Chip> :
            <Chip variant='outlined' label='Not in user favorites yet.'></Chip>
          }
          
        </CardContent>
    </Card>
  )
};
export default CocktailListCard;