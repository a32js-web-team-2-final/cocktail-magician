import React from 'react';
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography';
import BarRating from '../../components/Rating/BarRating';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import RoomIcon from '@mui/icons-material/Room';

const SingleBarInfo = ({name, phone, address, rating}) => {

  return (
    <Box style={{display: "flex", alignItems: "center", justifyContent: "space-between", gap: "20px", padding: "10px 60px"}} sx={{bgcolor: "primary.main", color: "white"}}>
      <Box style={{display: "flex", alignItems: "center", gap: "10px"}}>
        <Typography variant="h5">{name}</Typography>
        <BarRating sx={{color: "white"}} rating={rating}/>
      </Box>
      <Box style={{display: "flex", alignItems: "center", gap: "10px"}}>
        <LocalPhoneIcon />
        <Typography component="span">{phone}</Typography>
      </Box>
      <Box style={{display: "flex", alignItems: "center", gap: "10px"}}>
        <RoomIcon />
        <Typography component="span">{address}</Typography>
      </Box>
  </Box>
  )
}

export default SingleBarInfo;