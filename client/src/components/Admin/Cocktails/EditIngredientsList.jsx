import React, {useState, useEffect} from 'react';
import { Button, IconButton } from '@material-ui/core';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { addIngredients,removeIngredients} from '../../../services/admin-requests';
import LocalDrinkIcon from '@mui/icons-material/LocalDrink';
import Select from 'react-select';
import { getCocktailDetails } from '../../../services/public-requests';


const EditIngredientsList = ({cocktailId, ingredients}) => {

  const[cocktailDetails, setCocktailDetails] = useState({});
  const [cocktailIngredients, setCocktailIngredients] = useState([]);
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    (cocktailId !== undefined) && 
    getCocktailDetails(cocktailId)
      .then(data => {
        if (data?.message) {
          enqueueSnackbar(`Ooops...There was an error loading cocktail details!`, { 
            variant: 'error',
          });
          throw new Error(data.message);
        } else {
          setCocktailDetails(data)
          setCocktailIngredients(cocktailDetails?.ingredients?.map(ingredient => {
            return {'value': ingredient.id, 'label': ingredient.name}
          }) || null)
        }})
      .catch(e => console.log(e))
  }, [cocktailDetails, cocktailId, enqueueSnackbar]);

  const allIngredientsSorted = ingredients.map(ingredient => {
                                    return {'value': ingredient.id, 'label': ingredient.name}})
                                  .sort((a,b) => (a.label).localeCompare(b.label));


  const onChange = (value, action) => {
    switch (action.action) {
      case 'select-option':
        addIngredients(cocktailId, [action.option.label])
            .then(data => {
              enqueueSnackbar(`You have successfully added ${(action.option.label).toUpperCase()} to ${cocktailDetails.name}!`, { 
                            variant: 'success',
              });
            });
        break;
      // eslint-disable-next-line no-fallthrough
      case 'remove-value':
        removeIngredients(cocktailId, [action.removedValue.label])
            .then(data => {
              enqueueSnackbar(`You have successfully removed ${(action.removedValue.label).toUpperCase()} from ${cocktailDetails.name}!`, { 
                            variant: 'success',
              });
            })
        break;
      default:
        return value;
}};

  return (
    <div style={{"textAlign": "center"}}>
          <IconButton
            size="medium"
            onClick={handleClickOpen}
          >
          <LocalDrinkIcon/>
          </IconButton>
      <Dialog 
        open={open}
        onClose={handleClose}
        maxWidth='md'
        scroll='paper'
      >
        <DialogTitle>{cocktailDetails.name}: Edit ingredients </DialogTitle>
        <DialogContent style={{height:'250px'}}>
          <DialogContentText>
              Please, edit the cocktail ingredients below.
              Note that you may only include ingredients that are already in the list.
              If you want to create a new one, go to admin -> ingredients -> new ingredient.
          </DialogContentText>
          <Select 
            defaultValue={cocktailIngredients}
            isMulti
            isSearchable = {true}
            isClearable = {true}
            onChange={onChange}
            name="ingredients"
            options={allIngredientsSorted}
            className="basic-multi-select"
            classNamePrefix="select"
            maxMenuHeight={160}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Done</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditIngredientsList;