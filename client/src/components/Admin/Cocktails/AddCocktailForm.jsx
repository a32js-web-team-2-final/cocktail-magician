
import * as React from 'react';
import { Button} from '@material-ui/core';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { useFormik }from 'formik';
import { useState } from 'react';
import { cocktailValidationSchema } from '../../../common/validation-schemas';
import Select from 'react-select';
import { createCocktail } from '../../../services/admin-requests';
import { customSelectStyle } from '../../../common/custom-select-style';

const AddCocktailForm = ({ingredients}) => {
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();
  
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    formik.resetForm();
  };

  const allIngredientsSorted = ingredients
                                .map(ingredient => {
                                  return {'value': ingredient.id, 'label': ingredient.name}})
                                .sort((a,b) => (a.label).localeCompare(b.label));

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: '',
      image: null,
      ingredients: []
    },
    validationSchema: cocktailValidationSchema,
    onSubmit: (values) => {

      let formData = new FormData();
      formData.append('name', values.name);
      formData.append('image', values.image);

      values.ingredients.forEach(ingredient => {
        formData.append('ingredients[]', ingredient.label);
      });

      createCocktail(formData)
        .then(data =>  {
            console.log(data)
            handleClose();
            formik.resetForm()})
    },
  });

  return (
    <div style={{"textAlign": "center"}}>
      <Button 
        variant="contained"
        onClick={handleClickOpen} 
      >
      New Cocktail
      </Button>
      <Dialog 
        open={open}
        onClose={handleClose}
        component="form"
        onSubmit={formik.handleSubmit}
        >
        <DialogTitle>Add new cocktail</DialogTitle>
          <DialogContent  style={{height:'250px'}}>
            <DialogContentText>
              Please, fill out the form to create a new cocktail.
            </DialogContentText>
            <TextField
              autoFocus
              required
              margin="dense"
              id="name"
              name="name"
              placeholder="Cocktail name goes here..."
              label="Cocktail name"
              type="text"
              fullWidth
              variant="outlined"
              value={formik.values.name}
              onChange={formik.handleChange}
              error={formik.touched.name && Boolean(formik.errors.name)}
              helperText={formik.errors.name && String(formik.errors.name)}
            />
            <Select
              isMulti
              isSearchable = {true}
              isClearable = {true}
              onChange={(value) => formik.setFieldValue("ingredients", value)}
              name="ingredients"
              options={allIngredientsSorted}
              className="basic-multi-select"
              classNamePrefix="select"
              styles={customSelectStyle}
              maxMenuHeight={120}
            />
            <input 
              required
              accept="image/*"
              type="file" 
              id="image"
              name="image" 
              onChange={(event) => formik.setFieldValue("image", event.target.files[0])}>
            </input>
          </DialogContent>
          <DialogActions>
            <Button variant="contained" type="submit" disabled={!formik.dirty}>Add cocktail</Button>
            <Button onClick={handleClose}>Cancel</Button>
          </DialogActions>
      </Dialog>
    </div>
  );
};

export default AddCocktailForm;
