import React, {useState, useEffect} from 'react';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { useFormik }from 'formik';
import { cocktailValidationSchema } from '../../../common/validation-schemas';
import { IconButton, Button } from '@material-ui/core';
import EditIcon from '@mui/icons-material/Edit';
import { editCocktailDetails } from '../../../services/admin-requests';
import { getCocktailDetails } from '../../../services/public-requests';

const EditCocktailDetails = ({cocktailId}) => {

  const[cocktailDetails, setCocktailDetails] = useState({});
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();
  
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    formik.resetForm();
  };

  useEffect(() => {
    cocktailId && 
    getCocktailDetails(cocktailId)
      .then(data => {
        if (data?.message) {
          enqueueSnackbar(`Ooops...There was an error loading cocktail details!`, { 
            variant: 'error',
          });
          throw new Error(data.message);
        } else {
          setCocktailDetails(data);
        }})
      .catch(e => console.log(e))
  }, [cocktailDetails, cocktailId, enqueueSnackbar]);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: `${cocktailDetails.name}`,
      image: null,
    },
    validationSchema: cocktailValidationSchema,
    onSubmit: (values) => {

      let formData = new FormData();

      if (formik.initialValues.name === values.name && formik.initialValues.image === values.image) {
        enqueueSnackbar('No change in cocktail details detected!', { 
          variant: 'info'});
      } else if (formik.initialValues.name === values.name && formik.initialValues.image !== values.image) {
        formData.append("image", values.image)
        console.log(formData.values())
        enqueueSnackbar(`You have successfully changed the image for ${cocktailDetails.name}!`, { variant: 'success'});
      } else if (formik.initialValues.name !== values.name && formik.initialValues.image === values.image) {
        formData.append("name", values.name)
        enqueueSnackbar(`You have successfully changed the name for ${cocktailDetails.name}!`, { variant: 'success'});
      } else {
        formData.append("name", values.name)
        formData.append("image", values.image)
        enqueueSnackbar(`You have successfully changed the details for ${cocktailDetails.name}!`, { variant: 'success'});
      }
      editCocktailDetails(cocktailId, formData)
        .then(data => console.log(data))
      handleClose();
      formik.resetForm();
    }
  });
  
  return (
    <div style={{"textAlign": "center"}}>
          <IconButton
            size="medium"
            onClick={handleClickOpen}
          >
          <EditIcon/>
          </IconButton>

      <Dialog open={open} onClose={handleClose} component="form" onSubmit={formik.handleSubmit}>
        <DialogTitle>{cocktailDetails.name}: Edit details</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please, edit the cocktail details below.
          </DialogContentText>
          <TextField
            autoFocus
            required
            margin="dense"
            id="name"
            name="name"
            label="Cocktail name"
            type="text"
            fullWidth
            variant="outlined"
            value={formik.values.name || formik.initialValues.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.errors.name && String(formik.errors.name)}
          />
          <input 
            type="file" 
            id="image"
            name="image"
            onChange={(event) => formik.setFieldValue("image", event.target.files[0])}/>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" type="submit" disabled={!formik.dirty}>Submit</Button>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditCocktailDetails;