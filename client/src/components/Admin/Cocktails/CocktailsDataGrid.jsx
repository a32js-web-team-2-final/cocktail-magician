import { DataGrid } from '@mui/x-data-grid';
import React, {useState} from 'react';
import EditIngredientsList from './EditIngredientsList';
import EditCocktailDetails from './EditCocktailDetails';
import { ButtonGroup } from '@material-ui/core';

const CocktailsDataGrid = ({cocktails, ingredients}) => {

  const [selectionModel, setSelectionModel] = useState([]);

  const rows = cocktails.map( cocktail => {
    return({
      'id': cocktail.id,
      'name': cocktail.name,
      'favouriteCount': cocktail.favouriteCount,
    })
    });

  const columns = [
    {field: 'id', hide: true},

    { field: 'name',
      headerName: 'Cocktail name',
      headerAlign: 'center',
      flex: 1},
    
    { field: 'favouriteCount',
      headerName: 'In favorites (total)',
      headerAlign: 'center',
      align: 'center',
      flex: 0.5
    }
  ];

  return(
    <>
      {
        selectionModel[0] && 
        <div style={{ height:'60px'}} >
        <ButtonGroup>
          <EditCocktailDetails cocktailId={selectionModel[0]}/>
          <EditIngredientsList cocktailId={selectionModel[0]} ingredients={ingredients}/>
        </ButtonGroup>
        </div>
      }
      <div style={{ height: 600, width: '100%', marginTop: 8, position: 'center' }}>
        <DataGrid
          rows={rows} 
          columns={columns}
          height ={500}
          padding={10}
          headerAlign={'center'}
          rowHeight={50}
          headerHeight={70}
          
          rowsPerPage={20}
          disableColumnMenu={true}
          disableColumnSelector={true}
          checkboxSelection
          hideFooterSelectedRowCount
          selectionModel={selectionModel}
          onSelectionModelChange={(selection) => {
          if (selection.length > 1) {
            const selectionSet = new Set(selectionModel);
            const result = selection.filter((s) => !selectionSet.has(s));
            setSelectionModel(result);
          } else {
            setSelectionModel(selection);
          }
        }}
        />
      </div>
    </>
  );
};

export default CocktailsDataGrid;
