import React from 'react';
import { useState } from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { IconButton, Button } from '@material-ui/core';
import DeleteIcon from '@mui/icons-material/Delete';
import { getToken } from '../../../common/contexts/auth-context';

const DeleteIngredientForm = ({ingredient}) => {
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();
  

  const deleteIngredient = (ingredient) => {
    // console.log(ingredient)
    return fetch(`http://localhost:5000/ingredients/${ingredient.id}`, {
      method: 'DELETE',
      headers: {
        'Authorization' : `Bearer ${getToken()}`,
        // 'Content-type': 'application/json'
      },
      // body: JSON.stringify({"name":ingredient.name})
    })
    .then(response => {
      if(response.ok) {
        enqueueSnackbar(`You have successfully deleted ${ingredient.name} from the ingredient list!`, { 
          variant: 'success',
        })
        handleClose()
      } else {
        enqueueSnackbar(`Ooops...!There was an error deleting ${ingredient.name} ingredient!`, { 
            variant: 'error',
          });
        throw new Error(response.message);
      }
    })
    .catch(e => console.log(e.message));
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  return (
    <div style={{"textAlign": "center"}}>
      <IconButton
        disabled={!!ingredient.cocktailsCount}
        onClick={handleClickOpen}>
        <DeleteIcon/>
      </IconButton>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Delete ingredient</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to delete this ingredient? 
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={()=> deleteIngredient(ingredient)}>Yes</Button>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DeleteIngredientForm;
