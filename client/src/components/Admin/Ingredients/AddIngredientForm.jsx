import * as React from 'react';
import { Button } from '@material-ui/core';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { useFormik }from 'formik';
import { useState } from 'react';
import { ingredientValidationSchema } from '../../../common/validation-schemas';
import { API } from '../../../common/constants';
import { getToken } from '../../../common/contexts/auth-context.js'

const AddIngredientForm = () => {
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    formik.resetForm();
  };

  const createIngredient = (name) => {
    return fetch(`${API}ingredients`, {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${getToken()}`,
          'Content-type': 'application/json'
        },
        body: JSON.stringify(name)
      })
      .then(data => data.json())
      .catch(console.error)
};
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: '',
    },
    validationSchema: ingredientValidationSchema,
    onSubmit: (values) => {
      createIngredient(values)
      .then(data =>  { 
        if (data.message) {
          console.log(`${data.message}`)
          enqueueSnackbar(`Ooops...There was an error adding a new ingredient!`, { 
            variant: "error",
          })
          throw new Error(data.message)
        } else  {
        console.log(data)
        enqueueSnackbar('You have successfully added a new ingredient!', { 
          variant: "success",
        })
      }
      })
    .catch(e => console.log(e))
      formik.resetForm()
      handleClose()
    },
  });

  return (
    <div style={{"textAlign": "center"}}>
      <Button 
        variant="contained"
        onClick={handleClickOpen} 
      >
      New Ingredient
      </Button>
      <Dialog open={open} onClose={handleClose} component="form" onSubmit={formik.handleSubmit}>
        <DialogTitle>Add new ingredient</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please, fill out the form to create a new ingredient, which you may use in multiple cocktails.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            name="name"
            placeholder="Ingredient name goes here..."
            label="Ingredient name"
            type="text"
            fullWidth
            variant="outlined"
            value={formik.values.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.errors.name && String(formik.errors.name)}
          />
        </DialogContent>
        <DialogActions>
          <Button variant="contained" type="submit" disabled={!formik.dirty}>Add ingredient</Button>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default AddIngredientForm;
