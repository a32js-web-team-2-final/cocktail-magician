import { DataGrid} from '@mui/x-data-grid';
import React from 'react';
import DeleteIngredientForm from './DeleteIngredientForm';


const IngredientsDataGrid = ({ingredients}) => {

  const rows = ingredients.map(ingredient => {
    return(
      { 'id': ingredient.id,
        'name': ingredient.name,
        'cocktailsCount': ingredient.cocktailsCount
      }
    );
  });

  const columns = [

    { field: 'name',
      headerName: 'Ingredient name', 
      headerAlign: 'center',
      flex: 1
    },
    { field: 'cocktailsCount',
      headerName: 'Cocktails (total)',
      align: 'center',
      headerAlign: 'center',
      flex: 0.5
    },
    {
      field: 'delete',
      headerName: 'Delete',
      sortable: false,
      align: 'center',
      headerAlign: 'center',
      flex: 0.3,
      renderCell: (cellValues) => {
        return (
          <DeleteIngredientForm ingredient={cellValues.row}/>
        );
      }
    }
  ];

  return(
    <div style={{ height: 500, width: '100%', marginTop: 8, position: 'center'}}>
      <DataGrid
        height={400}
        rowHeight={40}
        rows={rows}
        headerHeight={70}
        columns={columns}
        rowsPerPage={20}
        disableColumnMenu
        hideFooterSelectedRowCount
      />
    </div>
  );
};

export default IngredientsDataGrid;
