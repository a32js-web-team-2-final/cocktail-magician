import { DataGrid} from '@mui/x-data-grid';
import React, {useState} from 'react';
import EditBarDetails from './EditBarDetails';
import EditCocktailsList from './EditCocktailsList';
import UploadBarImagesDialog from './UploadBarImagesDialog';
import BarGalleryDialog from './BarGallery/BarGalleryDialog';
import { ButtonGroup} from '@material-ui/core';

const BarDataGrid = ({bars, cocktails}) => {

  const [selectionModel, setSelectionModel] = useState([]);

  const rows = bars.map(bar => {
    return({
      'id': bar.id,
      'name': bar.name,
      'phone': bar.phone, 
      'address': bar.address, 
      'rating': bar.rating, 
      'reviewsCount': bar.reviewsCount})
    });

  const columns = [
    { field: 'id',
      hide: true
    },
    { field: 'name',
      headerName: 'Bar name',
      headerAlign: 'center',
      flex: 0.4
    },
    { field: 'phone',
      headerName: 'Phone', 
      headerAlign: 'center',
      flex: 0.3 
    },
    { field: 'address',
      headerName: 'Address',
      headerAlign: 'center',
      flex: 0.5
    },
    { field: 'rating',
      headerName: 'Rating',
      headerAlign: 'center',
      align: 'center',
      flex: 0.2
    },
    { field: 'reviewsCount',
      headerName: 'Reviews (total)',
      headerAlign: 'center',
      align: 'center',
      flex: 0.2
    },
  ];

  return(
    <>
    {
      selectionModel[0] && 
      <div style={{ height:'60px'}} >
      <ButtonGroup>
        <EditBarDetails barId={selectionModel[0]}/>
        <UploadBarImagesDialog barId={selectionModel[0]}/>
        <BarGalleryDialog barId={selectionModel[0]}/>
        <EditCocktailsList barId={selectionModel[0]} cocktails={cocktails}/>
      </ButtonGroup>
      </div>
    }
    <div style={{ height:700, maxWidth: '100%', marginTop: 8, position: 'center' }}>
      <DataGrid
        height ={700}
        padding={10}
        headerAlign={'center'}
        rowHeight={50}
        headerHeight={70}
        rows={rows} 
        columns={columns}
        rowsPerPage={20}
        disableColumnMenu={true}
        disableColumnSelector={true}
        checkboxSelection
        hideFooterSelectedRowCount
        selectionModel={selectionModel}
        onSelectionModelChange={(selection) => {
        if (selection.length > 1) {
          const selectionSet = new Set(selectionModel);
          const result = selection.filter((s) => !selectionSet.has(s));
          setSelectionModel(result);
        } else {
          setSelectionModel(selection);
        }
      }}
      />
    </div>
    </>
  );
};

export default BarDataGrid;
