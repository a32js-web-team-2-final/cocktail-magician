import React, { useEffect } from 'react';
import { Button } from '@material-ui/core';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { useState } from 'react';
import { IconButton} from '@material-ui/core';
import LocalBarIcon from '@mui/icons-material/LocalBar';
import Select from 'react-select';
import { getBarDetails, addCocktailToBar, removeCocktailFromBar } from '../../../services/admin-requests';


const EditCocktailsList = ({barId, cocktails}) => {
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();
  const [existingCocktails, setExistingCocktails] = useState([]);


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    barId &&
    getBarDetails(barId)
      .then(data => data?.cocktails.map(cocktail => {
        return {'value': cocktail.id, 'label': cocktail.name}
      }))
      .then(data => setExistingCocktails(data))
  }, [existingCocktails, barId]);


  const allCocktailsSorted = cocktails
                              .map(cocktail => {
                                  return {'value': cocktail.id, 'label': cocktail.name}})
                              .sort((a,b) => (a.label).localeCompare(b.label));

  const onChange = (value, action) => {
    switch (action.action) {
      case 'select-option':
        addCocktailToBar(barId, action.option.value)
            .then(data => {
              // console.log(data);
              enqueueSnackbar(`You have successfully added ${(action.option.label).toUpperCase()} to the bar cocktails list!`, { 
                            variant: 'success',
              });
            });
        break;
      // eslint-disable-next-line no-fallthrough
      case 'remove-value':
        // console.log(action.removedValue) //id
        removeCocktailFromBar(barId, action.removedValue.value)
            .then(data => {
              // console.log(data);
              enqueueSnackbar(`You have successfully removed ${(action.removedValue.label).toUpperCase()} from the bar!`, { 
                            variant: 'info',
              });
            });
        break;
      default:
        return value;
}};
                              
  return (
    <div style={{"textAlign": "center"}}>
          <IconButton
            size="medium"
            onClick={handleClickOpen}
          >
          <LocalBarIcon/>
          </IconButton>
      <Dialog 
        open={open}
        onClose={handleClose}
        maxWidth='md'
        scroll='paper'
      >
        <DialogTitle> Edit bar cocktails </DialogTitle>
        <DialogContent style={{height:'220px'}}>
          <DialogContentText>
              Please, edit the bar cocktails below.
              Note that you may only include cocktails that are already in the list.
              If you want to create a new one, go to admin -> cocktails -> new cocktail.
          </DialogContentText>
          <Select 
            defaultValue={existingCocktails}
            isMulti
            isSearchable = {true}
            isClearable = {true}
            onChange={onChange}
            name="cocktails"
            options={allCocktailsSorted}
            className="basic-multi-select"
            classNamePrefix="select"
            maxMenuHeight={130}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Done</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditCocktailsList;