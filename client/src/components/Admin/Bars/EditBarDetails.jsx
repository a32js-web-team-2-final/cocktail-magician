import React, {useState, useEffect} from 'react';
import { Button, IconButton } from '@material-ui/core';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { useFormik }from 'formik';
import { barValidationSchema } from '../../../common/validation-schemas';
import EditIcon from '@mui/icons-material/Edit';
import { editBarDetails, getBarDetails} from '../../../services/admin-requests';


const EditBarDetails = ({barId}) => {
  const[barDetails, setBarDetails] = useState({});
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();
  
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    formik.resetForm();
  };

  useEffect(() => {
    barId && 
    getBarDetails(barId)
      .then(data => {
        if (data?.message) {
          enqueueSnackbar(`Ooops...There was an error loading bar details!`, { 
            variant: 'error',
          });
          throw new Error(data.message);
        } else {
          setBarDetails(data)
          // console.log(barDetails)
        }})
      .catch(e => console.log(e))
  }, [barDetails, barId, enqueueSnackbar]);


  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: `${barDetails.name}`,
      phone: `${barDetails.phone}`,
      address: `${barDetails.address}`,
    },
    validationSchema: barValidationSchema,
    onSubmit: (values) => {
      // const barId = barDetails.id;

      editBarDetails(barId, values)
        .then(data => {
          //console.log(data)
          handleClose()
          enqueueSnackbar(`You have successfully updated the bar details for ${barDetails.name}!`, { 
                  variant: 'success',
          })
          setBarDetails(data);
        })
        .catch(e => {
          console.log(e.message)
          enqueueSnackbar('Ooops...! There was an error updating bar details!', { 
            variant: 'error',
          })
        })
    }
  });

  return (
    <div style={{"textAlign": "center"}}>
          <IconButton
            size="medium"
            onClick={handleClickOpen}
          >
          <EditIcon/>
          </IconButton>
      <Dialog open={open} onClose={handleClose} component="form" onSubmit={formik.handleSubmit}>
        <DialogTitle>{barDetails.name}: Edit bar details</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please, edit the bar details below.
          </DialogContentText>
          <TextField
            required
            margin="dense"
            id="name"
            name="name"
            label="Bar name"
            type="text"
            fullWidth
            variant="outlined"
            value={formik.values.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.errors.name && String(formik.errors.name)}
          />
          <TextField
            autoFocus
            required
            margin="dense"
            id="phone"
            label="Bar phone"
            type="text"
            minRows={1}
            fullWidth
            variant="outlined"
            value={formik.values.phone}
            onChange={formik.handleChange}
            error={formik.touched.phone && Boolean(formik.errors.phone)}
            helperText={formik.errors.phone && String(formik.errors.phone)}
          />
          <TextField
            autoFocus
            required
            margin="dense"
            id="address"
            label="Bar address"
            type="text"
            multiline
            minRows={1}
            maxRows={3}
            fullWidth
            variant="outlined"
            value={formik.values.address}
            onChange={formik.handleChange}
            error={formik.touched.address && Boolean(formik.errors.address)}
            helperText={formik.errors.address && String(formik.errors.address)}
          />
        </DialogContent>
        <DialogActions>
          <Button variant="contained" type="submit" disabled={!formik.dirty}>Submit</Button>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditBarDetails;