
import React from 'react';
import { useState } from 'react';
import { IconButton, Button} from '@material-ui/core';
import { useSnackbar } from 'notistack';
import { useFormik }from 'formik';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Box } from '@mui/system';
import DriveFolderUploadIcon from '@mui/icons-material/DriveFolderUpload';
import { uploadMoreBarImages } from '../../../services/admin-requests';

const UploadBarImagesDialog = ({barId}) => {
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    formik.resetForm();
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      file: null,
    },
    onSubmit: (values) => {
      const fd = new FormData();
      console.log(values.file)
        fd.append("image", values.file);
        
      uploadMoreBarImages(barId, fd)
        .then(data =>  {
          if (data?.message) {
            enqueueSnackbar(`Ooops...There was an error uploading an additional image!`, { 
              variant: 'error',
            });
            throw new Error(data.message);
          } else {
            enqueueSnackbar(`You have successfully uploaded an image to the bar gallery!`, { 
              variant: 'success',
            });
            formik.resetForm();
            handleClose();
          }
        })
      .catch(e => console.log(e))
    }
  });

  return(
    <div style={{"textAlign": "center"}}>
      <IconButton onClick={handleOpen}>
        <DriveFolderUploadIcon/>
      </IconButton>
      <Dialog 
        open={open}
        onClose={handleClose}
        width='auto'
        scroll='paper'
        component="form"
        onSubmit={formik.handleSubmit}
      >
        <DialogTitle>Upload additional image</DialogTitle>
        <DialogContent>
        <Box alignItems='center' display='flex' justifyContent='center' flexDirection='column'>
        <Box>
          <input
            type='file'
            accept="image/*"
            id="file" 
            name="file"
            onChange={(event) => formik.setFieldValue("file", event.target.files[0])}
            />
        </Box>
        </Box>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" type="submit" disabled={!formik.dirty}>Upload</Button>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default UploadBarImagesDialog;


