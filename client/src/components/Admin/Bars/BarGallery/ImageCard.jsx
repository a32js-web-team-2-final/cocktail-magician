import { Grid } from "@material-ui/core";
import { Card, CardMedia, CardContent, CardActions  } from "@mui/material";
import { Chip, Fab} from "@material-ui/core";
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { toggleImageVisibility, setCoverImage } from "../../../../services/admin-requests";
import { useSnackbar } from 'notistack';
import PhotoFilterIcon from '@mui/icons-material/PhotoFilter';

const ImageCard = ({barId, images}) => {

  const { enqueueSnackbar } = useSnackbar();

  const toggleVisibility = (barId, imageId, action) => {
    if(action === 'show') {
      toggleImageVisibility(barId, imageId, 'show' )
      
      enqueueSnackbar('You have successfully returned this image to the bar gallery!', { 
        variant: 'success',
      })
    } else {
      toggleImageVisibility(barId, imageId, 'hide')
      enqueueSnackbar('You have successfully hidden this bar image!', { 
        variant: 'success',
      })
    }
  };

  const setBarCover = (barId, imageId) => {
    setCoverImage(barId, imageId)
      .then(data => { 
        //console.log(data)
        enqueueSnackbar('You have successfully set this image as a bar cover!', { 
          variant: 'success',
        })
      })
      .catch(e => {
        enqueueSnackbar('Oops! There was an error setting this image as a bar cover!', { 
          variant: 'error',
        })
      })
  };

  return (
    <>
    {!images.length ? 
      <div> No images </div> :
      images.map((image) => (
        <Grid item key={image.id} xs={12} sm={6} md={4} m={8} p={8}>
          <Card sx={{ height: '450px', width: 'auto', display: 'flex', flexDirection: 'column'}}>
            <CardMedia
              component="img"
              image={`http://localhost:5000${image.url}`}
              alt={"bar-image"}
              height="350px"
              width="auto"
            />
            <CardContent>
            {
              (image.isCover === 1) ?
              <Chip label="cover"/> :
              <></>
            }
            </CardContent>
            <CardActions> 
              {
                (!image.isCover && image.isVisible) ?
                <Fab onClick={()=> {toggleVisibility(barId, image.id, 'hide')}} variant="extended">
                <VisibilityOffIcon sx={{ mr: 1 }} />
                Hide
                </Fab> :
                null
              }
              {
                !image.isVisible &&
                <Fab onClick={()=> {toggleVisibility(barId, image.id, 'show')}} variant="extended" >
                <VisibilityIcon sx={{ mr: 1 }} />
                Show
                </Fab>
              }
              {
                (!image.isCover) ?
                <Fab variant="extended" onClick={() => {setBarCover(barId, image.id)}}>
                <PhotoFilterIcon sx={{ mr: 1 }}/> Set as cover</Fab> :
                null
              }
            </CardActions>
          </Card>
          </Grid>
        ))}
  </>
  );
};

export default ImageCard;

