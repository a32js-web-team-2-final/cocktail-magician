import React, { useEffect, useState } from 'react';
import { IconButton, Button, Grid} from '@material-ui/core';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import PhotoCameraIcon from '@mui/icons-material/PhotoCamera';
import { getBarDetails} from '../../../../services/admin-requests.js'
import ImageCard from './ImageCard.jsx';

const BarGalleryDialog = ({barId}) => {

  const [open, setOpen] = useState(false);
  const [images, setImages] = useState([]);

  useEffect(() => {
    barId &&
    getBarDetails(barId)
      .then(data => data.images)
      .then(data => setImages(data))
      .catch(e => console.log(e.message))
  }, [images, barId])


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  
  return (
    <div style={{"textAlign": "center"}}>
      <IconButton
        size="medium"
        onClick={handleClickOpen}
      >
        <PhotoCameraIcon/>
      </IconButton>
      <Dialog 
        open={open}
        onClose={handleClose}
        maxWidth='md'
        scroll='paper'
        height='auto'
        width='900'
      >
        <DialogTitle>Edit image visibility and cover</DialogTitle>
        <DialogContent>
        <DialogContentText> 
        You may hide and show bar images by 
        clicking on the button below or you may set up 1 image
        as a cover photo (it will be displayed when browsing for bars).
        </DialogContentText>
          <Grid container spacing={2}>
            <ImageCard barId={barId} images={images}/>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Done</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default BarGalleryDialog;