
import React, {useState} from 'react';
import { Button } from '@material-ui/core';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { useFormik }from 'formik';
import { barValidationSchema } from '../../../common/validation-schemas';
import { createBar } from '../../../services/admin-requests'

const AddBarForm = () => {
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    formik.resetForm();
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: '',
      phone: '',
      address: '',
      files: [],
    },
    validationSchema: barValidationSchema,
    onSubmit: (values) => {
      const formData = new FormData();

      formData.append('name', values.name);
      formData.append('phone', values.phone);
      formData.append('address', values.address);
      values.files.forEach(image => {
        formData.append('images', image);
      })
      createBar(formData)
        .then(data => {
          if (data?.message) {
          enqueueSnackbar(`Ooops...There was an error creating a new bar!`, { 
            variant: 'error',
          });
          throw new Error(data.message);
        } else {
          enqueueSnackbar(`You have successfully added a new bar!`, { 
            variant: 'success',
            });
            formik.resetForm();
            handleClose();
        }})
        .catch(e => console.log(e));
    }
  });

  return (
    <div style={{"textAlign": "center"}}>
      <Button 
        variant="contained"
        onClick={handleClickOpen} 
      >
      New Bar
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        component="form"
        onSubmit={formik.handleSubmit}>
        <DialogTitle>Add new bar</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please, fill out the form to create a new bar.
          </DialogContentText>
          <TextField
            autoFocus
            required
            margin="dense"
            id="name"
            name="name"
            placeholder="Bar name goes here..."
            label="Bar name"
            type="text"
            fullWidth
            variant="outlined"
            value={formik.values.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.errors.name && String(formik.errors.name)}
          />
          <TextField
            autoFocus
            required
            margin="dense"
            id="phone"
            placeholder="Bar phone goes here..."
            label="Bar phone"
            type="text"
            minRows={1}
            fullWidth
            variant="outlined"
            value={formik.values.phone}
            onChange={formik.handleChange}
            error={formik.touched.phone && Boolean(formik.errors.phone)}
            helperText={formik.errors.phone && String(formik.errors.phone)}
          />
          <TextField
            autoFocus
            required
            margin="dense"
            id="address"
            placeholder="Bar address goes here..."
            label="Bar address"
            type="text"
            multiline
            minRows={1}
            maxRows={3}
            fullWidth
            variant="outlined"
            value={formik.values.address}
            onChange={formik.handleChange}
            error={formik.touched.address && Boolean(formik.errors.address)}
            helperText={formik.errors.address && String(formik.errors.address)}
          />
          <input 
            type="file" 
            id="files"
            name="files" 
            multiple
            onChange={(event) => formik.setFieldValue("files", Array.from(event.target.files))}>
          </input>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" type="submit" disabled={!formik.dirty}>Add bar</Button>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default AddBarForm;
