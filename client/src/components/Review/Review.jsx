import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box'
import { Typography } from '@material-ui/core';
import { getUserById } from '../../services/public-requests';
import BarRating from '../Rating/BarRating';
import UserProfileTab from '../User/UserProfileTab';

const Review = ({review}) => {
  const [userOfReview, setUserOfReview] = useState([])
  const { displayName, username } = review.author;
  useEffect(() => {
    getUserById(review.author.id)
    .then(data => setUserOfReview(data))
  }, [review.author.id])

  return (
    <Box className="review" style={{display: "flex", flexDirection: "column", gap: "20px"}}>
      <Box style={{display: "flex", alignItems: "center", gap: "20px"}}>
          <Box sx={{display: "flex", flexDirection: "column"}}>
            <UserProfileTab user={userOfReview}/>
            <Typography>{review.author && (review.author.displayName || review.author.username)}</Typography>
          </Box>
        <BarRating rating={review.rating} />
      </Box>
      <Typography sx={{maxWidth: "620px"}}>{review.text}</Typography>
    </Box>
  )
}
export default Review;