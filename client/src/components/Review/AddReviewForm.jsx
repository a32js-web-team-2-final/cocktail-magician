import React, { useContext, useState} from 'react';
import { Button, Typography } from '@material-ui/core';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { useFormik }from 'formik';
import { reviewValidationSchema } from '../../common/validation-schemas';
import { Rating } from '@mui/material';
import { Tooltip } from '@mui/material';
import { addReview } from '../../services/private-requests.js';

const AddReviewForm = ({barId, hasReviewed, setHasReviewed}) => {
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    formik.resetForm();
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      text: '',
      rating: 0,
    },
    validationSchema: reviewValidationSchema,
    onSubmit: (values) => {
      values.rating = +values.rating // parse to number 

      addReview(barId, values)
      .then(data =>  {

        setHasReviewed(true);
        enqueueSnackbar('You have successfully submitted your review!', { 
          variant: 'success',
        });
        handleClose();
      })
      .catch(e => {
        console.log(e)
        enqueueSnackbar('Ooops...There was an error sending your review!', { 
          variant: 'error',
        })
      })
    }});

  return (
    <div style={{"textAlign": "center"}}>
      {hasReviewed ? 
      <Tooltip title="You have already reviewed this bar">
        <span>
          <Button variant="contained" disabled >Write review</Button>
        </span>
      </Tooltip>
      :
      <Button variant="contained" className="review-btn" onClick={handleClickOpen} disabled={hasReviewed}> Write review</Button>
    }
    
      <Dialog open={open} onClose={handleClose} component="form" onSubmit={formik.handleSubmit}>
        <DialogTitle>Add new review</DialogTitle>
        <DialogContent>
          <Typography component="legend" >Your rating: </Typography>
          <Rating
              name="rating"
              value={+(formik.values.rating)}
              onChange={formik.handleChange}
          />
          <TextField
            autoFocus
            required
            margin="dense"
            id="text"
            name="text"
            placeholder="Bar review goes here..."
            label="Your review"
            type="text"
            fullWidth
            multiline
            minRows={3}
            variant="outlined"
            value={formik.values.text}
            onChange={formik.handleChange}
            error={formik.touched.text && Boolean(formik.errors.text)}
            helperText={formik.errors.text && String(formik.errors.text)}
          />
          <DialogContentText >
            Thank you for sharing your experience!
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" type="submit" disabled={!formik.dirty}>Submit</Button>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default AddReviewForm;
