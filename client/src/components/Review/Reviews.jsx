import React, { useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Grid from '@mui/material/Grid';
import Typography from '@material-ui/core/Typography';
import Review from '../Review/Review';
import { getBarReviews } from '../../services/public-requests';
import AddReviewForm from './AddReviewForm';
import { useContext } from 'react';
import UserProfileContext from '../../common/contexts/user-profile-context';
import AuthContext from '../../common/contexts/auth-context';


const Demo = styled('div')(({ theme }) => ({
  backgroundColor: theme.palette.background.paper,
}));

const Reviews = ({id}) => {
  const [reviews, setReviews] = useState([]);
  const { isLoggedIn } =useContext(AuthContext);
  const { userProfile } = useContext(UserProfileContext);
  const [hasReviewed, setHasReviewed] = useState(false);

  useEffect(() => {
    getBarReviews(id)
    .then(data => {
      setReviews(data)

      if (data.some(({author}) => author.id === userProfile.id)) {
        setHasReviewed(true)
      }
    })
    
  }, [id, hasReviewed, userProfile]);

  return (
    <Box sx={{ minWidth: 700}}>
      <Grid container>
        <Grid item >
          <Typography sx={{ mt: 4, mb: 2, textAlign: "left" }} variant="h5" component="div">
            Reviews
          </Typography>
          <Demo>
            <List >
              {reviews.length ?
                reviews.map(review => (
                <ListItem key={review.id} sx={{fontSize: "1.2rem"}}>
                  <Review review={review} />
                </ListItem>))
              : 
              <Typography>This bar hasn't been reviewed yet.</Typography>
              }
            </List>
            {
              isLoggedIn && 
              <AddReviewForm barId={id} hasReviewed={hasReviewed} setHasReviewed={setHasReviewed}/>
            }
            
          </Demo>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Reviews;