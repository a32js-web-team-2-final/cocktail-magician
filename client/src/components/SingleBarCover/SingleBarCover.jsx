import React from 'react';

const SingleBarCover = ({images}) => {
  const cover = images.filter(image => image.isCover)[0]?.url;

  return (
    <div className="single-bar-cover" style={{backgroundImage: `linear-gradient(rgba(255,255,255,0.1), rgba(255,255,255,0.3)), url(${`http://localhost:5000${cover}`})`, height: "350px"}}></div>
  )
};

export default SingleBarCover;