import React from 'react';
import { Box } from '@mui/system';
import { Typography, Button } from '@material-ui/core';
import { useNavigate } from 'react-router-dom';
import BarCocktailList from '../../components/BarCocktailList/BarCocktailList'


const BarsByIngredientListItem = ({bar}) => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(`/bars/${bar.id}`)

  }

  // console.log(bar)

  return (
    <Box className="bar-list-item">
      <Box className="bar-info">
        <Typography className="bar-name" variant="h5">{bar.name}</Typography>
        <Box className="bar-address">
          <Typography>Address: {bar.address}</Typography>
        </Box>
        <Box className="bar-phone">
          <Typography>Phone: {bar.phone}</Typography>
        </Box>
        <Button className="bar-more-btn" sx={{bgcolor: "primary.main", color: "secondary.main"}} onClick={handleClick}>View more</Button>
      </Box>
      <BarCocktailList cocktails={bar.cocktails}/>
    </Box>
  )
}

export default BarsByIngredientListItem;