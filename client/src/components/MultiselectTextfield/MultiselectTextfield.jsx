import React, { useState } from 'react';
import CreatableSelect from 'react-select/creatable';
import Box from '@mui/material/Box';
import { Button } from '@mui/material';

const components = {
  DropdownIndicator: null,
};

const createOption = (label) => ({
  label,
  value: label,
});

const MultiselectTextfield = ({setIngredients}) => {
  const [values, setValues] = useState({
    inputValue: '',
    value: [],
  })

  const { inputValue, value } = values;

  const handleChange = (
    value,
    actionMeta
  ) => {
    // console.log(value)
    // console.group('Value Changed');
    // console.log(value);
    // console.log(actionMeta)
    // console.log(`action: ${actionMeta.action}`);
    // console.groupEnd();
    setValues(prev => ({...prev, value }));
    const ingredients = value.reduce((ingredients, value) => [...ingredients, value.value], [])
    setIngredients(ingredients)
  };

  const handleInputChange = (inputValue) => {
    setValues(prev => ({...prev, inputValue}));
  };

  const handleKeyDown = (event) => {
    
    const { inputValue, value } = values;
    
    if (!inputValue) return;
    switch (event.key) {
      case 'Enter':
      case 'Tab':
        // console.group('Value Added');
        // console.log(value);
        // console.groupEnd();
        setValues({
          inputValue: '',
          value: [...value, createOption(inputValue)],
        });
        event.preventDefault();
      break;
      default:
        return value
    }
  };

  const handleClick = () => {
    const { value } = values;
    const ingredients = value.reduce((ingredients, value) => [...ingredients, value.value], [])
    // console.log(ingredients);
    setIngredients(ingredients)
  }
    
    return (
      <Box sx={{display: "flex", gap: "10px", marginTop: "50px", marginBottom: "20px"}}>
      <CreatableSelect
        components={components}
        inputValue={inputValue}
        isClearable
        isMulti
        menuIsOpen={false}
        onChange={handleChange}
        onInputChange={handleInputChange}
        onKeyDown={handleKeyDown}
        placeholder="Type in and press enter to add ingredient..."
        value={value}
      />
      <Button variant="contained" className="search-btn" sx={{alignSelf: "center"}}onClick={handleClick}>Search</Button>
      </Box>
    );
}

export default MultiselectTextfield;