import * as React from 'react';
import { styled } from '@mui/material/styles';
import { Box } from '@mui/system';
import { List, ListItem, Grid, Typography } from '@material-ui/core';
import {useNavigate} from 'react-router-dom';

const Demo = styled('div')(({ theme }) => ({
  backgroundColor: theme.palette.background.paper,
}));

const BarCocktailList = ({barId, cocktails}) => {
  const navigate = useNavigate();

  const handleCocktailClick = (id) => {
    navigate(`/cocktails/${id}`)
  }

  return (
    <Box sx={{ flexGrow: 1, maxWidth: 752}}>
      <Grid container >
        <Grid item xs={12} md={6}>
          <Typography sx={{ mb: 2, textAlign: "left" }} variant="h5" component="div">
            Cocktails
          </Typography>
          <Demo>
            <List>
              {cocktails.length ?
              cocktails.map(cocktail => (
                <ListItem key={cocktail.id} sx={{fontSize: "1.2rem", display: "flex", flexDirection: "column", alignItems: "flex-start", maxWidth: "100%"}}>
                  <Box sx={{display: "flex", gap: "10px", alignItems: "center", cursor: "pointer"}} onClick={() => handleCocktailClick(cocktail.id)}>
                    <Box sx={{backgroundImage: `url(http://localhost:5000${cocktail.imageUrl})`, backgroundSize: 'contain', height: "80px", width: "80px"}}></Box>
                      <Typography sx={{padding: 0}}>{cocktail.name}</Typography>
                  </Box>
                </ListItem>
              ))
              :
              <Typography>This bar has no cocktails listed yet.</Typography>
            }
            </List>
          </Demo>
        </Grid>
      </Grid>
    </Box>
  );
}

export default BarCocktailList;