
import * as React from 'react';
import { Button, IconButton } from '@material-ui/core';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { useFormik }from 'formik';
import { useState, useEffect, useContext } from 'react';
import { displayNameValidationSchema } from '../../common/validation-schemas';
import { editDisplayName, getUserInfoMe } from '../../services/private-requests';
import EditIcon from "@mui/icons-material/Edit";
import UserProfileContext from "../../common/contexts/user-profile-context";

const EditDisplayNameForm = ({ userId }) => {
  const { userProfile, setUserProfile } = useContext(UserProfileContext);
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar} = useSnackbar();
  const [displayName, setDisplayName] = useState('');

  useEffect(() =>
    getUserInfoMe()
      .then(data => setDisplayName(data.displayName)),
      [displayName, userProfile]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    formik.resetForm();
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      displayName: `${displayName || ''}`,
    },
    validationSchema: displayNameValidationSchema,
    onSubmit: (values) => {
      console.log(values);
      editDisplayName(values)
        .then((data) => {
          setUserProfile((prev) => ({
            userProfile: {
              ...prev.userProfile,
              displayName: formik.values.displayName,
            },
          }));
          enqueueSnackbar("You have successfully changed your nickname!", {
            variant: "success",
          });
          handleClose();
        })
        .catch((e) => {
          console.log(e.message);
          enqueueSnackbar(
            "Ooops...There was an error updating your nickname!",
            {
              variant: "error",
            }
          );
        });
      formik.resetForm()
    }
  });

  return (
    <div style={{ textAlign: "center" }}>
      <IconButton onClick={handleClickOpen}>
        <EditIcon />
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        component="form"
        onSubmit={formik.handleSubmit}
      >
        <DialogTitle>Edit my nickname</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please, edit your nickname as desired.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="displayName"
            name="displayName"
            label="Edit nickname "
            type="text"
            fullWidth
            variant="outlined"
            value={formik.values.displayName}
            onChange={formik.handleChange}
            error={
              formik.touched.displayName && Boolean(formik.errors.displayName)
            }
            helperText={
              formik.errors.displayName && String(formik.errors.displayName)
            }
          />
        </DialogContent>
        <DialogActions>
          <Button variant="contained" color="inherit" type="submit">
            Update
          </Button>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditDisplayNameForm;
