import React from "react";
import { Divider, List, ListItem} from "@material-ui/core";
import { Rating } from "@mui/material";
import ReviewsIcon from '@mui/icons-material/Reviews';
import { Chip } from "@material-ui/core";
import { Typography } from "@material-ui/core";
import { Box } from "@mui/system";


const ReviewedBarsList = ({bars}) => {

  return(
    <>
    {bars.length === 1 ? 
        <Chip label='1 reviewed bar' icon={<ReviewsIcon/>}/>:
        <Chip label={`${bars.length} reviewed bars`} icon={<ReviewsIcon/>}/>
      }
      <List>
      {bars.map(bar =>
          <ListItem key={bar.id}>
            <Box sx={{display: "flex", flexDirection: "column", gap: "10px"}}>
              <Box sx={{display: "flex", alignItems: "center"}}>
                <Typography>{bar.name}</Typography>
                <Rating value={bar.myRating} readOnly/>
              </Box>
              <Typography>Review:</Typography>
              <Typography style={{ wordWrap: 'break-word' }}>{bar.text}</Typography>
            </Box>
            <Divider/>
          </ListItem>
      )}
      </List>
    </>
  );
};

export default ReviewedBarsList;
