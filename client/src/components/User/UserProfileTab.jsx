import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import { Box } from '@mui/system';
import { Dialog, DialogTitle, Tooltip } from '@mui/material';
import FavoriteIcon from '@mui/icons-material/Favorite';
import PersonIcon from '@mui/icons-material/Person';
import EditIcon from '@mui/icons-material/Edit';
import FavoriteCocktailsList from './FavoriteCocktailsList';
import ReviewedBarsList from './ReviewedBarsList';
import UserProfileCard from './UserProfileCard';
import IconButton from '@mui/material/IconButton';
import Avatar from '@mui/material/Avatar';
import AuthContext from '../../common/contexts/auth-context';


const UserTabPanel = (props) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box flex sx={{ p: 2 }}>
          <Typography component="div">{children}</Typography>
        </Box>
      )}
    </div>
  );
}

UserTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

const a11yProps = (index) => {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const UserTabPanelDialog = ({user}) => {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(0);
  const { isLoggedIn } = useContext(AuthContext);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
    {user.username &&
      <>
        <Tooltip title={(user.displayName || user.username)}>
        <IconButton
          size="large"
          edge="end"
          color="inherit"
          onClick={handleClickOpen}
          disabled = {!isLoggedIn}
        >
          <Avatar sx={{width: "36px", height: "36px"}}>
            {(user.displayName || user.username)[0]}
          </Avatar>
        </IconButton>
      </Tooltip>
        <Dialog open={open} onClose={handleClose}>
        <DialogTitle> {`${user.displayName || user.username}'s`} profile </DialogTitle>
        <Box
        sx={{ bgcolor: 'background.paper', display: 'flex',  height: 440, minWidth: 1380, }}
      >
        <Tabs
          orientation="vertical"
          // variant="scrollable"
          value={value}
          onChange={handleChange}
          sx={{ borderRight: 4, borderColor: 'divider' }}
          padding={10}
          margin={10}
        >
          <Tab icon={<PersonIcon/>} label="PROFILE"  {...a11yProps(0)} />
          <Tab icon={<EditIcon />} label="REVIEWS" {...a11yProps(1)} />
          <Tab icon={<FavoriteIcon />} label="FAVORITES"  {...a11yProps(2)} />
        </Tabs>
        <UserTabPanel value={value} index={0}>
          <UserProfileCard userId={user.id} username={user.username} role={user.role} displayName={user.displayName}/>
        </UserTabPanel>
        <UserTabPanel value={value} index={1}>
          {user.reviewedBars ?
          <ReviewedBarsList bars={user?.reviewedBars}/>
          :
          <p> No bars reviewed. </p>
          }
        </UserTabPanel>
        <UserTabPanel value={value} index={2}>
          {user.favouriteCocktails ?
          <FavoriteCocktailsList cocktails={user.favouriteCocktails}/>
          :
          <p> No favourite cocktails. </p>
          }
        </UserTabPanel>
      </Box>
      </Dialog>
      </>
    }
    </>
  );
};

export default UserTabPanelDialog;
