import React from "react";
import { Chip} from "@material-ui/core";
import AutoFixHighIcon from '@mui/icons-material/AutoFixHigh';
import ChildCareIcon from '@mui/icons-material/ChildCare';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import CampaignIcon from '@mui/icons-material/Campaign';
import Box from "@mui/material/Box";
import AuthContext from "../../common/contexts/auth-context";
import UserProfileContext from "../../common/contexts/user-profile-context";
import { useContext } from 'react';
import EditDisplayNameForm from "./EditDisplayNameForm";

const UserProfileCard= ({userId, username, role, displayName}) => {
  const { isLoggedIn } = useContext(AuthContext);
  const { userProfile } = useContext(UserProfileContext);

  return(
    <Box sx={{display: "flex", alignItems: "center"}}>
      <Chip label={`username: ${username}`} icon={<AccountCircleIcon/>}  variant="outlined" />
      { role === 'magician' ?
        <Chip label={`I'm a ${role}`} icon={<AutoFixHighIcon/>} /> :
        <Chip label={`I'm a ${role}`} icon={<ChildCareIcon/>} /> 
      }
      <Chip label={`nickname: ${displayName}`} icon={<CampaignIcon/>}  variant="outlined"/>
      {(isLoggedIn && userId === userProfile.id) &&
      <EditDisplayNameForm />
      }
    </Box>
  );
};


export default UserProfileCard;