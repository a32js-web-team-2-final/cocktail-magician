import React from "react";
import { List, ListItem} from "@material-ui/core";
import LocalBarIcon from "@mui/icons-material/LocalBar";
import { Chip } from "@material-ui/core";

const FavoriteCocktailsList = ({ cocktails }) => {
  return(
    <>
      {cocktails.length === 1 ? 
        <Chip label='1 favorite cocktail' icon={<LocalBarIcon/>}/>:
        <Chip label={`${cocktails.length} favorite cocktails`} icon={<LocalBarIcon/>}/>
      }
      <List>
      {cocktails.map(cocktail =>
          <ListItem key={cocktail.id} > {cocktail.name}</ListItem>
      )}
      </List>
    </>
  );
};

export default FavoriteCocktailsList;
