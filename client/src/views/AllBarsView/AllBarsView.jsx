import React, { useEffect, useState } from "react";
import './AllBarsView.css'
import BarListItem from '../../components/BarListItem/BarListItem';
import { Box } from "@mui/system";
import { Typography, Checkbox } from "@mui/material";
import { searchBarsByName, searchBarsByCocktailIngredient } from '../../services/public-requests';
import SearchInput from '../../components/Search/SearchInput';
import MultiselectTextfield from '../../components/MultiselectTextfield/MultiselectTextfield';
import BarsByIngredientListItem from "../../components/BarsByIngredientListItem/BarsByIngredientListItem";
import Loader from '../../components/Loader/Loader'

const AllBarsView = () => {
  const [bars, setBars] = useState([]);
  const [checked, setChecked] = useState(false); 
  const [name, setName] = useState('');
  const [ingredients, setIngredients] = useState([]);
  const [barsByIngredients, setBarsByIngredients] = useState([]);
  const [loading, setLoading] = useState(false)
  
  useEffect(() => {
    if(!(ingredients.length) || !checked) {
      setLoading(true);
      searchBarsByName(name)
      .then(data => setBars(data))
      .finally(() => setLoading(false))
    }
  }, [name, ingredients.length])

  useEffect(() => {
    if (ingredients.length && checked) {
      setBars([])
      setLoading(true);

      let params = ingredients.join('&values=')
      searchBarsByCocktailIngredient(params)
      .then(data => {
        let bars = new Map()
        const results = data.reduce((bars, cocktail) =>  [...bars, cocktail.bars],[])
        .flat();
  
        const barIds = new Set(results.map(bar => bar.barId))
        
        barIds.forEach(id => {
            const cocktails = results.filter((result, index) => result.barId === id)
            .reduce((cocktails, cocktail) => {
              if (cocktails.some(entry => entry.cocktailId === cocktail.cocktailId)) {
  
                return cocktails;
              }
              
              return [...cocktails, cocktail]
            }, [])
            
            const {barId, barName, phone, address} = cocktails[0];
  
            bars.set(barId, {
              id: barId,
              name: barName,
              phone,
              address,
              cocktails: cocktails.map(({cocktailId, cocktailName, cocktailImage}) => {
                return {id: cocktailId, name: cocktailName, imageUrl: cocktailImage}
              })
            })
        })
  
        //setBars([]);
        setBarsByIngredients(Array.from(bars, ([key, value]) => value))
  
      })
      .finally(() => setLoading(false));
    }

  }, [ingredients])

  const showLoader = () => {
    if (loading) {
      return <Loader />
    }
  }


  const handleChecked = (event) => {
    if (checked) {
      setIngredients([])
      setBarsByIngredients([])
    }
    setChecked(event.target.checked);
  }
  
  return(
    <Box className="view-container">
      <Box className='all-bars-view'>
        <Typography className='all-bars-view-title' variant="h4">BARS</Typography>
        {!checked && 
        <SearchInput checked={checked} ingredients={ingredients} setIngredients={setIngredients} name={name} setName={setName}/>
        }
        {checked && 
        <MultiselectTextfield setIngredients={setIngredients}/>
        }
        <Box className="search-checkbox">
          <Checkbox checked={checked} onClick={handleChecked}/>
          <Typography>Search by cocktail ingredient</Typography>
        </Box>
        {(name &&!(bars.length)) &&
        <Typography variant="h5" sx={{textAlign: "left"}}>No bars by the name of "{name}"</Typography>
        }
        {showLoader()}
        {bars.length > 0 &&
          <Box className="bar-list-container">
          {bars.map(bar => (
            <BarListItem key={bar.id} bar={bar}/>
          ))}
          </Box>
        }
        {(checked && ingredients.length > 0 && !barsByIngredients.length) &&
          <Typography variant="h5" sx={{textAlign: "left"}}>No bars found with cocktails containing "{ingredients.join(', ')}"</Typography>
        }
        {(barsByIngredients.length > 0 && ingredients.length > 0) &&
        <Box className="bar-list-container">
          <Typography variant="h5" sx={{textAlign: "left"}}>{barsByIngredients.length} bars found with cocktails containing "{ingredients.join(', ')}"</Typography>
          {barsByIngredients.map(bar => (
            <BarsByIngredientListItem bar={bar} />
            ))}
          </Box>
        }
      </Box>
    </Box>
  );
};

export default AllBarsView;
