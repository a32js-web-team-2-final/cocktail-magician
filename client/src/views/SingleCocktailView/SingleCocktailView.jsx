import React, { useContext, useEffect, useState } from "react";
import { Box } from "@material-ui/core";
import { useParams } from "react-router";
import { getCocktailDetails, getUserById } from "../../services/public-requests";
import SingleCocktailCard from '../../components/SingleCocktailCard/SingleCocktailCard';
import AuthContext from '../../common/contexts/auth-context';
import UserProfileContext from '../../common/contexts/user-profile-context';

const SingleCocktailView = () => {
  const { isLoggedIn } = useContext(AuthContext);
  const { userProfile } = useContext(UserProfileContext);
  const { id } = useParams()
  const [cocktail, setCocktail] = useState({});
  const [isFavourite, setIsFavourite] = useState(false);
  const [favouritedBy, setFavouritedBy] = useState([])
  
  useEffect(() => {
    getCocktailDetails(id)
    .then(data => {
      setCocktail(data)
      
      return data
    })
    .then((data) => {

      return Promise.all(data.favouritedBy.map(user => getUserById(user.id)))
      .then(data => setFavouritedBy(data));
    })
    .then(() => {
      if (isLoggedIn && userProfile.username) {
        if (userProfile.favouriteCocktails.some(fav => fav.id === cocktail.id)) {
          setIsFavourite(true);
        }
      }
    })
  }, [id, cocktail.id, userProfile.favouriteCocktails, isLoggedIn, userProfile.username])


  return (
    <Box className="view-container">
      <Box className="single-cocktail-view" sx={{paddingTop: "50px", paddingLeft: "60px"}}>
        <SingleCocktailCard 
        cocktail={cocktail} 
        isFavourite={isFavourite} 
        setIsFavourite={setIsFavourite} 
        favouritedBy={favouritedBy}/>
      </Box>
    </Box>
  );
};


export default SingleCocktailView;
