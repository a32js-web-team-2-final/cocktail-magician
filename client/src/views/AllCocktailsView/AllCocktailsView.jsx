import React from "react";
import { Box } from "@mui/system";
import { Typography } from "@mui/material";
import { Grid } from "@mui/material";
import CocktailListCard from '../../components/CocktailListCard/CocktailListCard';
import { useState, useEffect } from "react";
import { getAllCocktails } from '../../services/public-requests';
import Loader from "../../components/Loader/Loader"

const AllCocktailsView = () => {
  const [cocktails, setCocktails] = useState([]);
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    getAllCocktails()
    .then(data => setCocktails(data))
    .finally(() => setLoading(false));
  }, [])

  const showLoader = () => {
    if (loading) {
      return <Loader />
    }
  }

  return (
    <Box className="view-container">
      <Box className="all-cocktails-view" sx={{ flexGrow: 1 }}>
        <Typography variant="h4" sx={{textAlign: "left", marginBottom: "50px"}}> COCKTAILS </Typography>
        {showLoader()}
        <Grid container spacing={{ xs: 2, md: 3 }} >
          {cocktails && cocktails.map(cocktail => (
            <Grid item xs={12} sm={4} md={3} key={cocktail.id} >
              <CocktailListCard cocktail={cocktail} />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  );
};


export default AllCocktailsView;
