import React from "react";
import BannerLayout from '../components/Home/BannerLayout'

const HomeView = () => {
  return(
    <>
    <BannerLayout
      sxBackground={{
        backgroundImage: `url('${process.env.PUBLIC_URL}/assets/welcome.png')`,
        backgroundPosition: 'center',
      }}

    >
    </BannerLayout>
    <BannerLayout
      sxBackground={{
        backgroundImage: `url('${process.env.PUBLIC_URL}/assets/about.png')`,
        backgroundPosition: 'center',
      }}
    >
    </BannerLayout>
    <BannerLayout
      sxBackground={{
        backgroundImage: `url('${process.env.PUBLIC_URL}/assets/comingSoon.png')`,
        backgroundPosition: 'center',
      }}
    >
    </BannerLayout>
    </>
  );
};

export default HomeView;