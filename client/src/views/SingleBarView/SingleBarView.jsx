import React, {useState, useEffect} from "react";
import './SingleBarView.css'
import { getBarDetails} from '../../services/public-requests';
import { useParams } from 'react-router-dom';
import Gallery from '../../components/Gallery/Gallery'
import { Box, Typography } from "@material-ui/core";
import SingleBarCover from "../../components/SingleBarCover/SingleBarCover";
import SingleBarInfo from '../../components/SingleBarInfo/SingleBarInfo';
import BarCocktailList from '../../components/BarCocktailList/BarCocktailList';
import Reviews from '../../components/Review/Reviews';

const SingleBarView = () => {
  const [bar, setBar] = useState({images: [], reviews: [], cocktails: []});
  const { id } = useParams();
  
  useEffect(() => {
    getBarDetails(id)
    .then(data => {
      setBar(data);
    })
  }, [id])

  return(
    <Box sx={{bgcolor: "#666"}}>
      <Box className="single-bar-view" sx={{bgcolor: "#d9d9d9"}}>
          <SingleBarCover images={bar.images}/>
          <SingleBarInfo name={bar.name} phone={bar.phone} address={bar.address} rating={bar.rating} />
        <Box className="contact-reviews-container">
          <BarCocktailList style={{padding: 0}} cocktails={bar.cocktails}/>
          <Reviews id={id} />
        </Box>
        <Typography variant="h5" style={{textAlign: "left", marginLeft: "60px", paddingBottom: "16px"}}>Gallery</Typography>
        <Gallery bar={bar} images={bar.images} />
      </Box>
    </Box>
  );
};


export default SingleBarView;
