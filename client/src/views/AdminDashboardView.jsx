
import React, {useState, useEffect} from 'react';
import BarDataGrid from '../components/Admin/Bars/BarDataGrid';
import CocktailsDataGrid from '../components/Admin/Cocktails/CocktailsDataGrid';
import IngredientsDataGrid from '../components/Admin/Ingredients/IngredientsDataGrid';
import AddBarForm from '../components/Admin/Bars/AddBarForm'
import AddIngredientForm from '../components/Admin/Ingredients/AddIngredientForm';
import { Grid } from '@mui/material';
import { Box } from '@mui/system';
import AddCocktailForm from '../components/Admin/Cocktails/AddCocktailForm';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { getAllBars, getAllCocktails, getAllIngredients } from '../services/admin-requests';
import { useSnackbar } from 'notistack';


const AdminDashboardView = () => {
  const [allBars, setAllBars] = useState([]);
  const [allCocktails, setAllCocktails] = useState([]);
  const [allIngredients, setAllIngredients] = useState([]);
  const {enqueueSnackbar} = useSnackbar();
  
  useEffect(() => {
    getAllBars()
      .then(data => {
        if (data?.message) {
          enqueueSnackbar(`Ooops...There was an error loading all bars!`, { 
            variant: 'error',
          });
          throw new Error(data.message);
        } else {
          setAllBars(data);
        }})
      .catch(e => console.log(e))
  }, [allBars, enqueueSnackbar]);


  useEffect(() => {
    getAllCocktails()
      .then(data => {
        if (data?.message) {
          enqueueSnackbar(`Ooops...There was an error loading all cocktails!`, { 
            variant: 'error',
          });
          throw new Error(data.message);
        } else {
          setAllCocktails(data)
        }})
      .catch(e => console.log(e))
  }, [allCocktails, enqueueSnackbar]);

  useEffect(() => {
    getAllIngredients()
      .then(data => {
        if (data?.message) {
          enqueueSnackbar(`Ooops...There was an error loading all ingredients!`, { 
            variant: 'error',
          });
          throw new Error(data.message);
        } else {
          setAllIngredients(data)
        }})
      .catch(e => console.log(e))
  }, [allIngredients, enqueueSnackbar]);

  return (
    <Box minHeight={800}>
      <Typography variant="h3" gutterBottom mt={10}>  Magician's Dashboard </Typography> 
      <Typography variant="subtitle" gutterBottom> <i> 
      { 'Here, at Barology, magicians are in charge of managing the bars, cocktails, and ingredients. \n'}
      </i></Typography>
      <Typography><i>
      { 'Please, click on the arrows to view category-specific details, create new instances, and edit the existing database.'} 
      </i> </Typography> 
      <div style={{"textAlign": "left"}}>
      <Box m={20}>
      <Accordion style={{backgroundColor: "transparent"}} TransitionProps={{ unmountOnExit: true }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />} >
          <Typography variant="h5" m={2}> BARS</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Grid container>
            <AddBarForm />
          </Grid> 
            <BarDataGrid bars={allBars} cocktails={allCocktails}/> 
        </AccordionDetails>
      </Accordion>
      <Accordion style={{backgroundColor: "transparent"}} TransitionProps={{ unmountOnExit: true }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="h5" m={2}> COCKTAILS</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Grid container>
            <AddCocktailForm ingredients={allIngredients}/>
          </Grid>
        </AccordionDetails>
        <CocktailsDataGrid cocktails={allCocktails} ingredients={allIngredients}/>
      </Accordion>
      <Accordion style={{backgroundColor: "transparent"}} TransitionProps={{ unmountOnExit: true }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />} >
          <Typography variant="h5" m={2} textAlign={'left'}>INGREDIENTS</Typography>
        </AccordionSummary>
          <AccordionDetails>
            <Grid container>
              <AddIngredientForm />
            </Grid>
            <IngredientsDataGrid ingredients={allIngredients}/>
          </AccordionDetails>
      </Accordion>
      </Box>
    </div>
    </Box>
  );
};

export default AdminDashboardView;
