import React, {useState, memo} from 'react'
import { GoogleMap, LoadScript, Marker, InfoWindow} from '@react-google-maps/api';
import { googleMapsApiKey } from '../common/constants';
import { coords, center } from '../common/map-data';
import { Grid } from '@material-ui/core';

const containerStyle = {
  width: 'auto',
  height: '800px',
  padding: 10,
  margin: 10,
  border: '5px solid grey'
};



const  MapView = () => {
  const[selectedBar, setSelectedBar] = useState(null);
  return (
    <>
      <LoadScript googleMapsApiKey={googleMapsApiKey}>
        <GoogleMap
          mapContainerStyle={containerStyle}
          center={center}
          zoom={15}>
      {coords.map(bar => (
        <Marker
          key={bar.id}
          position={{
            lat: bar.lat,
            lng: bar.lng
          }}
          onClick={() => { setSelectedBar(bar); }}
        />
      ))
      }
      {selectedBar && (
        <InfoWindow
          onCloseClick={() => { setSelectedBar(null); }}
          position={{
            lat: selectedBar.lat,
            lng: selectedBar.lng
          }}
        >
          <Grid container heigh="100px">
            <Grid item>
              <h2>{selectedBar.name}</h2>
            </Grid>
          </Grid>
        </InfoWindow>
      )}
        </GoogleMap>
      </LoadScript>
    </>
  );
};


export default memo(MapView);
