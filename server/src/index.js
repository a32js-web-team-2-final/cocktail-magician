import express from 'express';
import cors from 'cors';
import passport from 'passport';
import passportJwt from 'passport-jwt';
import { secret } from './data/secret.js';
import users from './routes/users.js';
import bars from './routes/bars.js';
import ingredients from './routes/ingredients.js';
import cocktails from './routes/cocktails.js';

let port = 5000;
let app = express();

passport.use(new passportJwt.Strategy(
    { secretOrKey: secret, jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken() },
    (payload, done) => done(null, { id: payload.sub })
));

app.use(cors(), express.json(), passport.initialize());
app.use('/', express.static('public'));
app.use('/users', users);
app.use('/bars', bars);
app.use('/ingredients', ingredients);
app.use('/cocktails', cocktails);


app.use((err, req, res, next) => {
    if (err.fileError) {
        res.status(400).send({ message: err.message })
    } else {
        next(err);
    }
});

app.listen(port, () => {
    console.log(`Listening on http://localhost:${port}...`);
});
