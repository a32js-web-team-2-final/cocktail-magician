import { fileURLToPath } from 'url';
import { dirname, join } from 'path';
import sqlite3 from 'sqlite3';
import { roles } from './roles.js';
import bcrypt from 'bcrypt';

const errHandler = (err) => err && console.log(err);

const path = join(dirname(fileURLToPath(import.meta.url)), './cocktail_magician.db');
const db = new sqlite3.Database(path, errHandler);

export const tables = Object.freeze({
    users: 'users',
    bars: 'bars',
    cocktails: 'cocktails',
    ingredients: 'ingredients',
    barReviews: 'bars_reviews',
    barImages: 'bar_images',
    favouriteCocktails: 'users_cocktais',
    barsCocktails: 'bars_cocktais',
    cocktailsIngredients: 'cocktail_ingredients'
});

db.serialize(() => {
    const usersTableSql =
        `CREATE TABLE IF NOT EXISTS ${tables.users} (
            id INTEGER PRIMARY KEY,
            username TEXT NOT NULL UNIQUE,
            password TEXT NOT NULL,
            displayName TEXT,
            role TEXT NOT NULL DEFAULT ${roles.CRAWLER}
        );`

    const barsTableSql =
        `CREATE TABLE IF NOT EXISTS ${tables.bars} (
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL UNIQUE,
            address TEXT,
            phone TEXT
        );`

    const barsImagesTableSql =
        `CREATE TABLE IF NOT EXISTS ${tables.barImages} (
            id INTEGER PRIMARY KEY,
            url TEXT NOT NULL UNIQUE,
            isCover INTEGER(1) NOT NULL DEFAULT 0,
            isVisible INTEGER(1) NOT NULL DEFAULT 1,
            barId INT,
            FOREIGN KEY (barId)
                REFERENCES ${tables.bars} (id) 
        );`

    const barsReviewsTableSql =
        `CREATE TABLE IF NOT EXISTS ${tables.barReviews} (
            id INTEGER PRIMARY KEY,
            text TEXT NOT NULL,
            rating INTEGER NOT NULL,
            barId INTEGER,
            userId INTEGER,
            FOREIGN KEY (barId)
                REFERENCES ${tables.bars} (id) 
            FOREIGN KEY (userId)
                REFERENCES ${tables.users} (id) 
        );`

    const ingredientsTableSql =
        `CREATE TABLE IF NOT EXISTS ${tables.ingredients} (
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL UNIQUE
        );`

    const cocktailsTableSql =
        `CREATE TABLE IF NOT EXISTS ${tables.cocktails} (
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL UNIQUE,
            imageUrl TEXT NOT NULL
        );`

    const cocktailsIngredientsTableSql =
        `CREATE TABLE IF NOT EXISTS ${tables.cocktailsIngredients} (
            cocktailId INTEGER NOT NULL,
            ingredientId INTEGER NOT NULL,
            PRIMARY KEY(cocktailId,ingredientId)
            FOREIGN KEY (cocktailId)
                REFERENCES ${tables.cocktails} (id) 
            FOREIGN KEY (ingredientId)
                REFERENCES ${tables.ingredients} (id) 
        );`

    const favouriteCocktailsTableSql =
        `CREATE TABLE IF NOT EXISTS ${tables.favouriteCocktails} (
            cocktailId INTEGER NOT NULL,
            userId INTEGER NOT NULL,
            PRIMARY KEY (cocktailId, userId)
            FOREIGN KEY (cocktailId)
                REFERENCES ${tables.cocktails} (id) 
            FOREIGN KEY (userId)
                REFERENCES ${tables.users} (id) 
        );`

    const barsCocktailsTableSql =
        `CREATE TABLE IF NOT EXISTS ${tables.barsCocktails} (
            cocktailId INTEGER NOT NULL,
            barId INTEGER NOT NULL,
            PRIMARY KEY (cocktailId, barId)
            FOREIGN KEY (cocktailId)
                REFERENCES ${tables.cocktails} (id) 
            FOREIGN KEY (barId)
                REFERENCES ${tables.bars} (id) 
        );`

    db.run(usersTableSql, errHandler);
    db.run(barsTableSql, errHandler);
    db.run(barsImagesTableSql, errHandler);
    db.run(barsReviewsTableSql, errHandler);
    db.run(ingredientsTableSql, errHandler);
    db.run(cocktailsTableSql, errHandler);
    db.run(cocktailsIngredientsTableSql, errHandler);
    db.run(favouriteCocktailsTableSql, errHandler);
    db.run(barsCocktailsTableSql, errHandler);

    bcrypt
        .hash('admin', 10)
        .then(passHash => {
            db.run(`
                INSERT OR IGNORE INTO ${tables.users}(username, password, displayName, role) 
                VALUES('admin', '${passHash}', 'admin', '${roles.MAGICIAN}')`,
                errHandler
            );
        });
});

export const select = (sql, params = []) => new Promise((resolve, reject) => {
    db.all(sql, params, (err, rows) => {
        if (err) reject(err);
        else resolve(rows);
    });
});

export const insert = (sql, params = []) => new Promise((resolve, reject) => {
    db.run(sql, params, function (err) {
        if (err) reject(err);
        else resolve({ recordId: this.lastID });
    })
});

export const update = (sql, params = []) => new Promise((resolve, reject) => {
    db.run(sql, params, function (err) {
        if (err) reject(err);
        else resolve({ rowsAffected: this.changes });
    })
});
